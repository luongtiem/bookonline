//
//  FavoriteFix.swift
//  bookonline
//
//  Created by tubjng on 1/11/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift
class FavoriteFix: UIViewController {
    @IBOutlet var favoriteFixTable: UITableView!
    let baseURL = "http://bookonline.myaawu.com/"
    var realm: Realm?
    var favoriteBook = [FavoriteBook]()
    var recenltyBook = [RecentlyBook]()
    var favoriteBookOffline = [FavoriteBookOffline]()
    var recentlyBookOffline = [RecentlyBookOffline]()
    var indexFix = [Int]()
    var i = 0
    var checkView = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        favoriteFixTable.delegate = self
        favoriteFixTable.dataSource = self
        self.favoriteFixTable.register(UINib(nibName: "FavoriteFixCell", bundle: nil), forCellReuseIdentifier: "Cell")
        self.favoriteFixTable.register(UINib(nibName: "FavoriteSectionTitle", bundle: Bundle.main), forCellReuseIdentifier: "header")
    }
    override func viewWillAppear(_ animated: Bool) {
        if checkView == 0{
            realm = try? Realm()
            recenltyBook.removeAll()
            let objects = realm?.objects(RecentlyBook.self)
            for object in objects! {
                recenltyBook.append(object)
            }
        }
        else if checkView == 1{
            realm = try? Realm()
            favoriteBook.removeAll()
            let objects = realm?.objects(FavoriteBook.self)
            for object in objects! {
                favoriteBook.append(object)
            }
        }
        else if checkView == 2{
            realm = try? Realm()
            favoriteBookOffline.removeAll()
            let objects = realm?.objects(FavoriteBookOffline.self)
            for object in objects! {
                favoriteBookOffline.append(object)
            }
        }
        else {
            realm = try? Realm()
            recentlyBookOffline.removeAll()
            let objects = realm?.objects(RecentlyBookOffline.self)
            for object in objects! {
                recentlyBookOffline.append(object)
            }
        }


        favoriteFixTable.reloadData()
    }
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func buttonDeleteFavorite(_ sender: Any) {
        if checkView == 0{
            let objects = realm?.objects(RecentlyBook.self)
            for i in 0..<recenltyBook.count {
                if recenltyBook[i].checked {
                    print(recenltyBook[i].id)
                    for obj in objects! {
                        if obj.id == recenltyBook[i].id {
                            try! realm?.write {
                                realm?.delete(obj)
                            }
                            break
                        }
                    }
                }
            }
            while isContantChecked() {
                for index in 0 ... recenltyBook.count - 1 {
                    if recenltyBook[index].checked {
                        recenltyBook.remove(at: index)
                        break
                    }
                }
            }
            favoriteFixTable.reloadData()
        }
        else if checkView == 1{
        let objects = realm?.objects(FavoriteBook.self)
        for i in 0..<favoriteBook.count {
            if favoriteBook[i].checked {
                print(favoriteBook[i].id)
                for obj in objects! {
                    if obj.id == favoriteBook[i].id {
                        try! realm?.write {
                            realm?.delete(obj)
                        }
                        break
                    }
                }
            }
        }
        while isContantChecked() {
            for index in 0 ... favoriteBook.count - 1 {
                if favoriteBook[index].checked {
                    favoriteBook.remove(at: index)
                    break
                }
            }
        }
        favoriteFixTable.reloadData()
        }
        else if checkView == 2{
            let objects = realm?.objects(FavoriteBookOffline.self)
            for i in 0..<favoriteBookOffline.count {
                if favoriteBookOffline[i].checked {
                    print(favoriteBookOffline[i].id)
                    for obj in objects! {
                        if obj.id == favoriteBookOffline[i].id {
                            try! realm?.write {
                                realm?.delete(obj)
                            }
                            break
                        }
                    }
                }
            }
            while isContantChecked() {
                for index in 0 ... favoriteBookOffline.count - 1 {
                    if favoriteBookOffline[index].checked {
                        favoriteBookOffline.remove(at: index)
                        break
                    }
                }
            }
            favoriteFixTable.reloadData()
        }
        else {
            let objects = realm?.objects(RecentlyBookOffline.self)
            for i in 0..<recentlyBookOffline.count {
                if recentlyBookOffline[i].checked {
                    print(recentlyBookOffline[i].id)
                    for obj in objects! {
                        if obj.id == recentlyBookOffline[i].id {
                            try! realm?.write {
                                realm?.delete(obj)
                            }
                            break
                        }
                    }
                }
            }
            while isContantChecked() {
                for index in 0 ... recentlyBookOffline.count - 1 {
                    if recentlyBookOffline[index].checked {
                        recentlyBookOffline.remove(at: index)
                        break
                    }
                }
            }
            favoriteFixTable.reloadData()
        }

    }
    
    func isContantChecked() -> Bool {
        if checkView == 0 {
            if recenltyBook.count == 0 {
                return false
            } else {
                for book in recenltyBook {
                    if book.checked {
                        return true
                    }
                }
            }

        }
        else if checkView == 1{
        if favoriteBook.count == 0 {
            return false
        } else {
            for book in favoriteBook {
                if book.checked {
                    return true
                }
            }
        }
        }
        else if checkView == 2{
            if favoriteBookOffline.count == 0 {
                return false
            } else {
                for book in favoriteBookOffline {
                    if book.checked {
                        return true
                    }
                }
            }
        }

        else{
            if recentlyBookOffline.count == 0 {
                return false
            } else {
                for book in recentlyBookOffline {
                    if book.checked {
                        return true
                    }
                }
            }
        }

        return false
    }
    
    var selectedAll = false
    
    @IBAction func selectAllBook(_ sender: UIButton) {
        selectedAll = !selectedAll
        if checkView == 0{
            for book in recenltyBook {
                book.checked = selectedAll
            }
            favoriteFixTable.reloadData()
            sender.setTitle(selectedAll ? R.string.bookonline.unSelect() : R.string.bookonline.selectAll(), for: .normal)

        }
        else if checkView == 1{
        for book in favoriteBook {
            book.checked = selectedAll
        }
        favoriteFixTable.reloadData()
        sender.setTitle(selectedAll ? R.string.bookonline.unSelect() : R.string.bookonline.selectAll(), for: .normal)
    }
        else if checkView == 2{
            for book in favoriteBookOffline {
                book.checked = selectedAll
            }
            favoriteFixTable.reloadData()
            sender.setTitle(selectedAll ? R.string.bookonline.unSelect() : R.string.bookonline.selectAll(), for: .normal)
        }
        else {
            for book in recentlyBookOffline {
                book.checked = selectedAll
            }
            favoriteFixTable.reloadData()
            sender.setTitle(selectedAll ? R.string.bookonline.unSelect() : R.string.bookonline.selectAll(), for: .normal)
        }
    }
 
}
extension FavoriteFix: UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as? FavoriteSectionTitle
        if checkView == 0{
            realm = try? Realm()
            recenltyBook.removeAll()
            let objects = realm?.objects(RecentlyBook.self)
            for object in objects! {
                recenltyBook.append(object)
            }
        }
        else if checkView == 1{
            realm = try? Realm()
            favoriteBook.removeAll()
            let objects = realm?.objects(FavoriteBook.self)
            for object in objects! {
                favoriteBook.append(object)
            }
        }
        else if checkView == 2{
            realm = try? Realm()
            favoriteBookOffline.removeAll()
            let objects = realm?.objects(FavoriteBookOffline.self)
            for object in objects! {
                favoriteBookOffline.append(object)
            }
        }
        else {
            realm = try? Realm()
            recentlyBookOffline.removeAll()
            let objects = realm?.objects(RecentlyBookOffline.self)
            for object in objects! {
                recentlyBookOffline.append(object)
            }
        }

        if checkView == 0{
        cell?.numbBook.text = String(recenltyBook.count)
        }
        else if checkView == 1{
            cell?.numbBook.text = String(favoriteBook.count)
        }
        else if checkView == 2{
            cell?.numbBook.text = String(favoriteBookOffline.count)
        }
        else{
            cell?.numbBook.text = String(recentlyBookOffline.count)
        }
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if checkView == 0{
            recenltyBook[indexPath.row].checked = !recenltyBook[indexPath.row].checked
            self.favoriteFixTable.reloadRows(at: [indexPath], with: .none)
        }
        else if checkView == 1{
            favoriteBook[indexPath.row].checked = !favoriteBook[indexPath.row].checked
            self.favoriteFixTable.reloadRows(at: [indexPath], with: .none)

        }
        else if checkView == 2{
            favoriteBookOffline[indexPath.row].checked = !favoriteBookOffline[indexPath.row].checked
            self.favoriteFixTable.reloadRows(at: [indexPath], with: .none)
            
        }
        else {
            recentlyBookOffline[indexPath.row].checked = !recentlyBookOffline[indexPath.row].checked
            self.favoriteFixTable.reloadRows(at: [indexPath], with: .none)
            
        }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if checkView == 0 {
            return recenltyBook.count

        }
        else if checkView == 1
        {
            return favoriteBook.count

        }
        else if checkView == 2
        {
            return favoriteBookOffline.count
            
        }else{
            return recentlyBookOffline.count
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FavoriteFixCell
        cell.selectionStyle = .none
        if checkView == 0{
            let book = recenltyBook[indexPath.row]
            cell.titleBook.text = book.name
            cell.authorBook.text = book.author
            if book.checked {
                cell.imageCheck.image = UIImage(named: "CheckPoint")
            } else {
                cell.imageCheck.image = UIImage(named: "Circle")
            }
            let stringURL = recenltyBook[indexPath.item].image
            if stringURL != ""{
                let url = URL(string: "\(baseURL)\(stringURL)")
                cell.imageBook.kf.setImage(with: url)
            }
        }
        else if checkView == 1{
            let book = favoriteBook[indexPath.row]
            cell.titleBook.text = book.name
            cell.authorBook.text = book.author
            if book.checked {
                cell.imageCheck.image = UIImage(named: "CheckPoint")
            } else {
                cell.imageCheck.image = UIImage(named: "Circle")
            }
            let stringURL = favoriteBook[indexPath.item].image
            if stringURL != ""{
                let url = URL(string: "\(baseURL)\(stringURL)")
                cell.imageBook.kf.setImage(with: url)
            }
        }
        else if checkView == 2{
            let book = favoriteBookOffline[indexPath.row]
            cell.titleBook.text = book.name
            cell.authorBook.text = book.author
            if book.checked {
                cell.imageCheck.image = UIImage(named: "CheckPoint")
            } else {
                cell.imageCheck.image = UIImage(named: "Circle")
            }
            let stringURL = favoriteBookOffline[indexPath.item].image
            if stringURL != ""{
                let url = URL(string: "\(baseURL)\(stringURL)")
                cell.imageBook.kf.setImage(with: url)
            }
        }
        else{
            let book = recentlyBookOffline[indexPath.row]
            cell.titleBook.text = book.name
            cell.authorBook.text = book.author
            if book.checked {
                cell.imageCheck.image = UIImage(named: "CheckPoint")
            } else {
                cell.imageCheck.image = UIImage(named: "Circle")
            }
            let stringURL = recentlyBookOffline[indexPath.item].image
            if stringURL != ""{
                let url = URL(string: "\(baseURL)\(stringURL)")
                cell.imageBook.kf.setImage(with: url)
            }
        }
        return cell
    }
    

}
