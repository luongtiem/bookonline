//
//  AudioOfflineVC.swift
//  bookonline
//
//  Created by Enrik on 2/14/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift

class AudioOfflineVC: UIViewController {
    
    @IBOutlet weak var audioTableView: UITableView!
    
    let tableCellIdentifier = "OfflineTableCell"

    var offlineStories = [OfflineStory]()
    var stories = [OfflineStory]()
    
    var saveGenres: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        audioTableView.delegate = self
        audioTableView.dataSource = self
        
        audioTableView.tableFooterView = UIView()
        self.audioTableView.register(UINib(nibName: "AdsTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell1")
        let nib = UINib(nibName: "OfflineTableViewCell", bundle: nil)
        audioTableView.register(nib, forCellReuseIdentifier: tableCellIdentifier)

    }

    override func viewWillAppear(_ animated: Bool) {
        requestData()
    }
    
    func requestData() {
        let realm = try! Realm()
        
        self.stories = []
        self.offlineStories = []
        
        let objects = realm.objects(OfflineStory.self)
        
        if self.saveGenres.count > 0 {
            
            for object in objects {
                
                if object.book_type == "1" {
                    var index = 0
                    while (index < saveGenres.count) && (saveGenres[index] != object.category_name) {
                        index += 1
                    }
                    if index < saveGenres.count {
                        offlineStories.append(object)
                    }
                }
                
            }
            
        } else {

            for object in objects {
                if object.book_type == "1" {
                    self.offlineStories.append(object)
                }
            }
        }
        
        self.stories = self.offlineStories
        self.audioTableView.reloadData()
    }
    
    func deleteStory(_ offlineStory: OfflineStory, indexPath: IndexPath) {
        
        let realm = try! Realm()
        
        let cell = audioTableView.cellForRow(at: indexPath) as! OfflineTableViewCell
        
        if cell.isFavorited == true {
            let objects = realm.objects(FavoriteBookOffline.self)
            
            for object in objects {
                if object.id == offlineStory.id {
                    try! realm.write {
                        realm.delete(object)
                    }
                    break
                }
            }
        }

        var sumSize = 0
        for chapter in offlineStory.chapters {
            sumSize = sumSize + chapter.content.utf8.count
        }
        var imageSize = 0
        if offlineStory.imageData != nil {
            imageSize = (offlineStory.imageData?.length)!
        }
        let size = UserDefaults.standard.value(forKey: k_Size) as! Int
        let newSize = size - sumSize - imageSize
        UserDefaults.standard.set(newSize, forKey: k_Size)
        
        try! realm.write {
            realm.delete(offlineStory)
        }
        
        
        
    }
    
    func convert(from offlineStory: OfflineStory) -> TopBook {
        let topBook = TopBook()
        
        topBook.id = offlineStory.id
        topBook.name = offlineStory.name
        topBook.image = offlineStory.image
        topBook.description = offlineStory.bookDescription
        topBook.book_source = offlineStory.book_source
        topBook.author = offlineStory.author
        topBook.country_id = offlineStory.country_id
        topBook.category_id = offlineStory.category_id
        topBook.page_number = offlineStory.page_number
        topBook.publisher = offlineStory.publisher
        topBook.publish_date = offlineStory.publish_date
        topBook.create_date = offlineStory.create_date
        topBook.last_update = offlineStory.last_update
        topBook.isbn = offlineStory.isbn
        topBook.language_type = offlineStory.language_type
        topBook.book_type = offlineStory.book_type
        topBook.chap_number = offlineStory.chap_number
        topBook.num_view = offlineStory.num_view
        topBook.category_name = offlineStory.category_name
        
        return topBook
    }
}

extension AudioOfflineVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stories.count + (stories.count/5) 

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 5 == 0 && indexPath.row != 0{
            
            let cell = audioTableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! AdsTableViewCell
            return cell
            
        }else{
            let indexPatchRow = indexPath.row - (indexPath.row/5)
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier, for: indexPath) as! OfflineTableViewCell
        
        cell.configure(story: stories[indexPatchRow])
        
        return cell
            }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .destructive, title: "DELETE") { (action, index) in
            if indexPath.row % 5 == 0 && indexPath.row != 0{
                print("1234")
            }else{
            let indexPathRow = indexPath.row - (indexPath.row/5)
            let indexPath1 = NSIndexPath(row: indexPathRow, section: 0)
            self.removeDirectory(story: self.stories[indexPathRow])
            self.deleteStory(self.stories[indexPathRow], indexPath: indexPath1 as IndexPath)
            self.stories.remove(at: indexPathRow)

            self.requestData()
        }
            }
        return [edit]
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row % 5 == 0 && indexPath.row != 0{
            return false
        }
        else{
            return true
        }
    }

    
    func removeDirectory(story: OfflineStory) {

        for chapter in story.chapters {
            if chapter.file_audio != "" {
                
                if let dir1 = kDOCUMENT_DIRECTORY_PATH {
                    let path = dir1 + chapter.file_audio
                    
                    let attributes = try! FileManager.default.attributesOfItem(atPath: path)
                    let fileSize = attributes[FileAttributeKey.size] as! Int
                    
                    let size = UserDefaults.standard.value(forKey: k_Size) as! Int
                    let newSize = size - fileSize
                    UserDefaults.standard.set(newSize, forKey: k_Size)
                }
                
            }
        }
        
        
        if let dir = kDOCUMENT_DIRECTORY_PATH {
            do {
                let path = dir + "/\(story.name)"

                try FileManager.default.removeItem(atPath: path)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row % 5 == 0 && indexPath.row != 0{
            print("push sang browser")
        }else{
            let indexPathRow = indexPath.row - (indexPath.row/5)
        let storyboard = UIStoryboard(name: "Online", bundle: nil)
        let detailVC = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        
        let offlineStory = self.stories[indexPath.row]
        
        var bookChapters: [BookChapters] = []
        
        for chapter in offlineStory.chapters {
            var bookChapter = BookChapters()
            bookChapter.chap_name = chapter.chap_name
            bookChapter.chap_no = chapter.chap_no
            bookChapter.content = chapter.content
            bookChapter.file_audio = chapter.file_audio
            bookChapter.id = offlineStory.id
            
            bookChapters.append(bookChapter)
        }
        
        detailVC.isOffline = true
        detailVC.data = self.convert(from: offlineStory)
        detailVC.bookChapters = bookChapters
        detailVC.checkView = false
        detailVC.checkRecently = 1
        
        if let imageData = offlineStory.imageData {
            detailVC.imageStory = UIImage(data: imageData as Data)
        }
            
        detailVC.isAudioBook = true 
        
        self.navigationController?.pushViewController(detailVC, animated: true)
        
        if let offlineVC = self.parent as? OfflineVC {
            offlineVC.endSearchBar()
        }
    }
    }
}

