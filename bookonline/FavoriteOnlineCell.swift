//
//  FavoriteOnlineCell.swift
//  bookonline
//
//  Created by tubjng on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class FavoriteOnlineCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    @IBOutlet weak var imageBook: UIImageView!
    @IBOutlet weak var titleBook: UILabel!
    @IBOutlet weak var authorBook: UILabel!
    
    var books : TopBook?{
        
        didSet{
            titleBook.text = books?.name
            authorBook.text = books?.author
        }
    }
    
}
