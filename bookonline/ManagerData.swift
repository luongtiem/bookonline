//
//  ManagerData.swift
//  bookonline
//
//  Created by ReasonAmu on 1/12/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ManagerData: BaseService {
    
    
    static let instance = ManagerData()
    
    static let ALL_CATEGORY = "http://restbookonline.myaawu.com/rest/getAllCategory"
    static let KEY_CATEGORY = "categories"
    
    static let TOP_BOOK = "http://restbookonline.myaawu.com/rest/getTopBook"
    static let KEY_BOOKS = "books"
    
    static let DETAIL_BOOK = "http://restbookonline.myaawu.com/rest/getBookById"
    static let KEY_DETAIL_BOOK = "books"
    
    
    
    
}
enum BookType : Int {
    
    case text = 0
    case audio = 1
    case all = 2
}

extension ManagerData {
    
    
    //MARK: GET ALL CATEGORY
    
    func getAllCategory(success : @escaping (_ data : JSON?, _ msg : Int) -> (),
                        fail : @escaping (_ msg : Int) -> ()){
        
        request(url: ManagerData.ALL_CATEGORY, method: .get, params: nil,
                key_Data: ManagerData.KEY_CATEGORY,
                success: {  json, msg  in
                    
                    success(json, msg)
                    
        }) { msg  in
            fail(msg)
        }
        
    }
    
    
    //MARK: GET TOP BOOK
    
    func getTopBook(number_book : Int , from : Int , book_type : BookType,
                    success : @escaping (_ data : JSON?, _ msg : Int) -> (),
                    fail : @escaping (_ msg : Int) -> ()){
        
        let params: [String : Any] = [
            "top" : number_book,
            "from" : from,
            "book_type" : book_type.rawValue
        ]
        
       
        request(url: ManagerData.TOP_BOOK, method: .get,
                params: params,
                key_Data: ManagerData.KEY_BOOKS, success: { json, msg  in
                    
                    success(json, msg)
                    
        }) { msg  in
            fail(msg)
        }
        
    }
    
    
    //MARK: GET DETAIL BOOK
    
    func getDetailBook( book_id : String,
                        success : @escaping (_ data : JSON?, _ msg : Int) -> (),
                        fail : @escaping (_ msg : Int) -> ()){
        
        let params : [String: Any] = ["book_id" : book_id]
        
        request(url: ManagerData.DETAIL_BOOK, method: .get,
                params: params,
                key_Data: ManagerData.KEY_DETAIL_BOOK, success: {  json, msg  in
                    
                    success(json, msg)
                    
        }) { msg  in
            fail(msg)
        }
        
    }
    
    
    
    
}
