//
//  RecentlyOffline.swift
//  bookonline
//
//  Created by tubjng on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift
class RecentlyOffline: UIViewController {
    @IBOutlet var tableRecentlyOffline: UITableView!
    var realm: Realm?
    var recentlyBookOffline = [RecentlyBookOffline]()
    var stories = [RecentlyBookOffline]()
    let baseURL = "http://bookonline.myaawu.com/"
    
    override func viewDidLoad() {
        super.viewDidLoad()

            tableRecentlyOffline.delegate = self
            tableRecentlyOffline.dataSource = self
            tableRecentlyOffline.tableFooterView = UIView()
            self.tableRecentlyOffline.register(UINib(nibName: "RecentlyOnlineCell", bundle: nil), forCellReuseIdentifier: "Cell")
            self.tableRecentlyOffline.register(UINib(nibName: "FavoriteSectionTitle", bundle: Bundle.main), forCellReuseIdentifier: "header")
        NotificationCenter.default.addObserver(self, selector: #selector(searchBarRecentlyOfflineCancel), name: NSNotification.Name(rawValue: "searchBarRecentlyOfflineCancel"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(searchRecentOfflinelyDidChange), name: NSNotification.Name(rawValue: "searchRecentlyOfflineDidChange"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        realm = try? Realm()
        recentlyBookOffline.removeAll()
        let objects = realm?.objects(RecentlyBookOffline.self)
        for object in objects! {
            recentlyBookOffline.append(object)
        }
        self.stories = self.recentlyBookOffline
        tableRecentlyOffline.reloadData()
    }
    
    func searchBarRecentlyOfflineCancel() {
        self.stories = []
        self.stories = self.recentlyBookOffline
        self.tableRecentlyOffline.reloadData()
    }
    func searchRecentOfflinelyDidChange(notification:NSNotification) {
        if let textInput = notification.userInfo?["text"] as? String{
            self.stories = []
            for story in self.recentlyBookOffline {
                if story.name.uppercased().contains(textInput.uppercased()) {
                    self.stories.append(story)
                }
            }
            self.tableRecentlyOffline.reloadData()
            
        }
    }

    func convert(from offlineStory: OfflineStory) -> TopBook {
        let topBook = TopBook()
        
        topBook.id = offlineStory.id
        topBook.name = offlineStory.name
        topBook.image = offlineStory.image
        topBook.description = offlineStory.bookDescription
        topBook.book_source = offlineStory.book_source
        topBook.author = offlineStory.author
        topBook.country_id = offlineStory.country_id
        topBook.category_id = offlineStory.category_id
        topBook.page_number = offlineStory.page_number
        topBook.publisher = offlineStory.publisher
        topBook.publish_date = offlineStory.publish_date
        topBook.create_date = offlineStory.create_date
        topBook.last_update = offlineStory.last_update
        topBook.isbn = offlineStory.isbn
        topBook.language_type = offlineStory.language_type
        topBook.book_type = offlineStory.book_type
        topBook.chap_number = offlineStory.chap_number
        topBook.num_view = offlineStory.num_view
        topBook.category_name = offlineStory.category_name
        
        return topBook
    }
}
extension RecentlyOffline: UITableViewDelegate,UITableViewDataSource {
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let storyboard = UIStoryboard(name: "Online", bundle: nil)
            let detailVC = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
            
            let recentOffline = self.stories[indexPath.row]
            
            var offlineStory = OfflineStory()
            
            let realm = try! Realm()
            let objects = realm.objects(OfflineStory.self)
            for object in objects {
                if object.id == recentOffline.id {
                    offlineStory = object
                    break
                }
            }
            
            var bookChapters: [BookChapters] = []
            
            for chapter in offlineStory.chapters {
                let bookChapter = BookChapters()
                bookChapter.chap_name = chapter.chap_name
                bookChapter.chap_no = chapter.chap_no
                bookChapter.content = chapter.content
                bookChapter.file_audio = chapter.file_audio
                
                bookChapters.append(bookChapter)
            }
            
            detailVC.isOffline = true
            detailVC.data = self.convert(from: offlineStory)
            detailVC.bookChapters = bookChapters
            detailVC.checkView = false
            detailVC.checkRecently = 1
            
            if detailVC.data.book_type == Constant.BookTypeAudio {
                detailVC.isAudioBook = true
            } else {
                detailVC.isAudioBook = false
            }
            
            if let imageData = offlineStory.imageData {
                detailVC.imageStory = UIImage(data: imageData as Data)
            }

            
            navigationController?.pushViewController(detailVC, animated: true)
            if let recentlyVC = self.parent as? RecentlyVC {
                recentlyVC.endSearchBar()
            }

        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return stories.count
        }
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header") as? FavoriteSectionTitle
            cell?.numbBook.text = String(stories.count)
            return cell
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableRecentlyOffline.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecentlyOnlineCell
            cell.author.text = stories[indexPath.row].author
            cell.nameTitle.text = stories[indexPath.row].name
            cell.selectionStyle = .none
            cell.display(recentlyBookOffline: stories[indexPath.row])
            let stringURL = stories[indexPath.item].image
            if stringURL != ""{
                let url = URL(string: "\(baseURL)\(stringURL)")
                cell.imageBook.kf.setImage(with: url)
            }
            return cell
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 20
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 90
        }
        
        
}
