//
//  Categories.swift
//  bookonline
//
//  Created by ReasonAmu on 1/12/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import Foundation
import SwiftyJSON

class Categories : BaseParseJson {
    
    var id : String?
    var name: String?
    var description : String?
    var create_date: String?
    var last_update: String?
    

    
    override func parse(from json: JSON) {
        super.parse(from: json)
        
        if let id : String = json["id"].string {
            self.id = id
        }
        
        if let name : String = json["name"].string {
            self.name = name
        }
        
        if let description : String = json["description"].string {
            self.description = description
        }
        
        if let create_date : String = json["create_date"].string {
            self.create_date = create_date
        }
        
        if let last_update : String = json["last_update"].string {
            self.last_update = last_update
        }
        
    }
}
