//
//  OfflineFliterGenreCell.swift
//  bookonline
//
//  Created by Enrik on 1/11/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class OfflineFliterGenreCell: UICollectionViewCell {

    @IBOutlet weak var btnGenre: UIButton!
    
    let addStateColor = UIColor.blue
    let noneStateColor = UIColor.clear
    
    var state: StateButtonGenre = .none 
    var title = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnGenre.layer.cornerRadius = 8
        self.btnGenre.layer.borderWidth = 1
        self.btnGenre.layer.borderColor = UIColor.gray.cgColor
        self.btnGenre.layer.masksToBounds = true

    }

    @IBAction func actionButtonGenre(_ sender: UIButton) {
        if title != R.string.bookonline.aZ() {
            switch self.state {
            case .none:
                self.btnGenre.setTitle(title, for: .normal)
                self.btnGenre.backgroundColor = addStateColor
                self.btnGenre.setTitleColor(UIColor.white, for: .normal)
                self.state = .add
            default:
                self.btnGenre.setTitle(title, for: .normal)
                self.btnGenre.backgroundColor = noneStateColor
                self.btnGenre.setTitleColor(UIColor.lightGray, for: .normal)
                self.state = .none
            }
        }
    }
    
    func changeToStateAdd() {
        self.state = .add
        self.btnGenre.setTitle(title, for: .normal)
        self.btnGenre.backgroundColor = UIColor.blue
        self.btnGenre.setTitleColor(UIColor.white, for: .normal)
    }
}
