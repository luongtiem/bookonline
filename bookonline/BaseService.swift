//
//  BaseService.swift
//  bookonline
//
//  Created by ReasonAmu on 1/12/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class BaseService: NSObject {
    
    func request(url: String, method: Alamofire.HTTPMethod , params: [String : Any]?,
                 key_Data : String,
                 success: @escaping (_ data: JSON?, _ msg: Int) -> (),
                 fail: @escaping (_ msg: Int) -> ()) {
        Alamofire.request(url, method: method, parameters: params).responseJSON { [unowned self] response in
            switch response.result {
            case .success(let result):
                let json = JSON(result)
                let msg = json["code"].int
                let data = json["\(key_Data)"]
                if data != nil {
                    success(data, msg!)
                }else {
                    fail(msg!)
                }
            case .failure:
                
                break
            }
        }
        
    }
    
    
}

