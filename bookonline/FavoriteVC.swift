//
//  FavoriteVC.swift
//  bookonline
//
//  Created by ReasonAmu on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class FavoriteVC: BaseViewController,UIPopoverPresentationControllerDelegate {
    @IBOutlet var favoriteOnline: UIView!
    @IBOutlet var segmentedFavorite: UISegmentedControl!
    @IBOutlet var favoriteOffline: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favoriteOffline.isHidden = true
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        addButton2(imageName: "Pen")
    

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showview"{
        let controller = segue.destination
            controller.popoverPresentationController?.delegate = self
            controller.preferredContentSize = CGSize(width: view.frame.width, height: 250)
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    override func actionBtn2() {
        if segmentedFavorite.selectedSegmentIndex == 0 {
            let favoritefix = self.storyboard?.instantiateViewController(withIdentifier: "FavoriteFixVC") as! FavoriteFix
            favoritefix.checkView = 1
            self.present(favoritefix, animated: true, completion: nil)

        }
        else{
        let favoritefix = self.storyboard?.instantiateViewController(withIdentifier: "FavoriteFixVC") as! FavoriteFix
            favoritefix.checkView = 2
            self.present(favoritefix, animated: true, completion: nil)
        }
    }


    @IBAction func actionChangeViewFavorite(_ sender: Any) {
        if segmentedFavorite.selectedSegmentIndex == 0 {
            favoriteOffline.isHidden = true
            favoriteOnline.isHidden = false
        }
        else
        {
            favoriteOnline.isHidden = true
            favoriteOffline.isHidden = false
        }
    }
}
extension FavoriteVC {
    
    override func actionSearch(_ sender: UIBarButtonItem) {
        super.actionSearch(sender)
        
        
    }
    
    override func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        super.searchBarTextDidBeginEditing(searchBar)
        
        
    }
    
    override func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        super.searchBarCancelButtonClicked(searchBar)
        if segmentedFavorite.selectedSegmentIndex == 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SearchBarFavoriteCancel"), object: nil)
        }
        else{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "searchBarFavoriteOfflineCancel"), object: nil)

        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if segmentedFavorite.selectedSegmentIndex == 0{
        if searchBar.text != "" {
            let textInput:[String : String] = ["text": searchBar.text!]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SearchFavoriteDidChange"), object: nil, userInfo: textInput)
            }}
        else{
            let textInput:[String : String] = ["text": searchBar.text!]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "searchFavoriteOfflineDidChange"), object: nil, userInfo: textInput)
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.searchBar.isFirstResponder == true {
            endSearchBar()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        endSearchBar()
    }
}


