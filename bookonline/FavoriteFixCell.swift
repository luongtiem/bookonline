//
//  FavoriteFixCell.swift
//  bookonline
//
//  Created by tubjng on 1/11/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class FavoriteFixCell: UITableViewCell {
    @IBOutlet var imageBook: UIImageView!

    @IBOutlet var titleBook: UILabel!

    @IBOutlet var imageCheck: UIImageView!
    
    @IBOutlet var viewChangeCheckPoint: UIView!
    @IBOutlet var authorBook: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageCheck.image = UIImage(named: "Circle")
        viewChangeCheckPoint.isHidden == true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
