//
//  OfflineModel.swift
//  bookonline
//
//  Created by Enrik on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import Foundation
import RealmSwift

class OfflineChapter: Object {
    dynamic var content: String = ""
    dynamic var chap_name: String = ""
    dynamic var chap_no: String = ""
    dynamic var file_audio: String = ""
}

class OfflineStory: Object {
    
    dynamic var id: String = ""
    dynamic var name: String = ""
    dynamic var image: String = ""
    dynamic var bookDescription: String = ""
    dynamic var book_source: String = ""
    dynamic var author: String = ""
    dynamic var country_id: String = ""
    dynamic var category_id: String = ""
    dynamic var page_number: String = ""
    dynamic var publisher: String = ""
    dynamic var publish_date: String = ""
    dynamic var create_date: String = ""
    dynamic var last_update: String = ""
    dynamic var isbn: String = ""
    dynamic var language_type: String = ""
    dynamic var book_type: String = ""
    dynamic var chap_number: String = ""
    dynamic var num_view: String = ""
    dynamic var category_name: String = ""
    dynamic var imageData: NSData? = nil 
    var chapters = List<OfflineChapter>()
}
