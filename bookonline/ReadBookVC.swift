//
//  ReadBookVC.swift
//  bookonline
//
//  Created by ReasonAmu on 1/14/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import PullToRefreshKit

class ReadBookVC: UIViewController,UIWebViewDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate ,UINavigationBarDelegate{
    var isOffline: Bool = false 
    var bookChapters : [BookChapters] = []
    var idBook: String?
    var dataHMTL : String?
    var setupContent : SetupContent?
    var chapters : BookChapters?
    lazy var settingLauncher : SetupContent  = {
        let setting =  SetupContent()
        setting.readBook = self
        return setting
    }()
    var numberIndexChapter : Int = 0
    var check : Bool = true
    

    @IBOutlet weak var loading: UIActivityIndicatorView!
    var viewTitleShow : UIView = {
        var viewTitle = UIView()
        viewTitle.backgroundColor = UIColor(red: 67/255, green:78/255, blue:93/255, alpha:1.0)
        viewTitle.translatesAutoresizingMaskIntoConstraints = false
        return viewTitle
        
    }()
    
    var btnMenu : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage( #imageLiteral(resourceName: "menu") , for: .normal)
        button.addTarget(self, action: #selector(handleMenu), for: .touchUpInside)
        return button
    }()
    
    var labelTitle : UILabel = {
        let lable = UILabel()
        lable.font = UIFont.systemFont(ofSize: 14)
        lable.textColor = UIColor.white
        lable.sizeToFit()
        lable.backgroundColor = UIColor.clear
        lable.numberOfLines = 1
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    var size : String!
    var color : String!
    var backGround : String!
    var font_Family : String!
    var loadDefaultLocal = UserDefaults.standard
    
    @IBOutlet weak var webViews : UIWebView!

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if loadDefaultLocal.value(forKey: "font_Family") == nil {
            size = "100%"
            color = "black"
            backGround = "white"
            font_Family = "Andada-Regular"
            
            loadDefaultLocal.setValue(size, forKey: "size")
            loadDefaultLocal.setValue(color, forKey: "color")
            loadDefaultLocal.setValue(backGround, forKey: "backGround")
            loadDefaultLocal.setValue(font_Family, forKey: "font_Family")
            
            
        }else{
            
            size = loadDefaultLocal.value(forKey: "size") as! String!
            color = loadDefaultLocal.value(forKey: "color") as! String!
            backGround = loadDefaultLocal.value(forKey: "backGround") as! String!
            font_Family = loadDefaultLocal.value(forKey: "font_Family") as! String!
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isStatusBarHidden = true
        UIApplication.shared.statusBarStyle = .lightContent
        webViews.isOpaque = false
        webViews.scrollView.delegate = self
        webViews.isUserInteractionEnabled = true
        _ = webViews.scrollView.setUpHeaderRefresh {
            if self.numberIndexChapter != 0 {
                self.viewTitleShow.removeFromSuperview()
                self.numberIndexChapter -= 1
                self.chapters = self.bookChapters[self.numberIndexChapter]
                self.setupUI(size: self.size, color: self.color, background: self.backGround, font_Family: self.font_Family,chapter : self.chapters!)
                
            }else{
                
//                self.alertView(title: R.string.bookonline.thisIsLastPage(), ok: R.string.bookonline.oK())
            }
            self.webViews.scrollView.endHeaderRefreshing(.success, delay: 0.3)
            }.SetUp { (header) in
                header.setText(R.string.bookonline.pullDown(), mode: .pullToRefresh)
                header.setText(R.string.bookonline.pullDown(), mode: .releaseToRefresh)
                header.setText(R.string.bookonline.success(), mode: .refreshSuccess)
                header.setText(R.string.bookonline.loading(), mode: .refreshing)
                header.setText(R.string.bookonline.error(), mode: .refreshFailure)
                header.textLabel.textColor = UIColor.lightGray
                header.imageView.image = nil
                
        }
        
        
        _ =  webViews.scrollView.setUpFooterRefresh {
            if self.numberIndexChapter < self.bookChapters.count  {
                self.viewTitleShow.removeFromSuperview()
                self.chapters = self.bookChapters[self.numberIndexChapter]
                self.setupUI(size: self.size, color: self.color, background: self.backGround, font_Family: self.font_Family,chapter : self.chapters!)
                self.numberIndexChapter += 1
            }else{
                
//                self.alertView(title: R.string.bookonline.thisIsLastPage(), ok: R.string.bookonline.oK())
            }
            self.webViews.scrollView.endFooterRefreshing()
            }.SetUp { (footer) in
                footer.setText(R.string.bookonline.pullDown(), mode: .pullToRefresh)
                footer.setText(R.string.bookonline.nextPage(), mode: .scrollAndTapToRefresh)
                footer.setText(R.string.bookonline.loading(), mode: .refreshing)
                footer.textLabel.textColor = UIColor.lightGray
                
                
        }
        
        let tapShowTitle = UITapGestureRecognizer(target: self, action: #selector(handleSlideMenu))
        tapShowTitle.numberOfTapsRequired = 1
        tapShowTitle.delegate = self
        
        view.addGestureRecognizer(tapShowTitle)
        loading.startAnimating()
        loadDetailBook()

    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        loadDefaultLocal.set(size, forKey: "size")
        loadDefaultLocal.set(color, forKey: "color")
        loadDefaultLocal.set(backGround, forKey: "backGround")
        loadDefaultLocal.set(font_Family, forKey: "font_Family")
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
        
    }
    
//    override var prefersStatusBarHidden: Bool = false
//    prefestat
    
    func handleSlideMenu(gesture : UITapGestureRecognizer){
        
        if check {
            UIApplication.shared.isStatusBarHidden = false
            addSubviews()
            
        }else{
            UIApplication.shared.isStatusBarHidden = true

            viewTitleShow.removeFromSuperview()
            view.superview?.updateFocusIfNeeded()
        }
        check = !check
        
    }
    
    func addSubviews(){
        
        view.addSubview(viewTitleShow)
        viewTitleShow.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        viewTitleShow.rightAnchor.constraint(equalTo: view.rightAnchor).isActive  = true
        viewTitleShow.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        viewTitleShow.heightAnchor.constraint(equalToConstant: 60).isActive = true
       
        
        viewTitleShow.addSubview(btnMenu)
        btnMenu.leftAnchor.constraint(equalTo: viewTitleShow.leftAnchor, constant: 10).isActive = true
        btnMenu.topAnchor.constraint(equalTo: viewTitleShow.topAnchor, constant: 10).isActive = true
        btnMenu.bottomAnchor.constraint(equalTo: viewTitleShow.bottomAnchor, constant: 10).isActive  = true
        btnMenu.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        viewTitleShow.addSubview(labelTitle)
        labelTitle.leftAnchor.constraint(equalTo: btnMenu.rightAnchor, constant: 10).isActive = true
        labelTitle.centerYAnchor.constraint(equalTo: btnMenu.centerYAnchor, constant: 0).isActive = true
        labelTitle.rightAnchor.constraint(equalTo: viewTitleShow.rightAnchor, constant: -8).isActive  = true
        labelTitle.text = chapters?.chap_name
        
    }
    
    
    func handleMenu(){
        
        slideMenuController()?.openLeft()
    }
    
    func loadData(){
        
        let url = Bundle.main.url(forResource: "dataHTML", withExtension: "html", subdirectory: nil, localization: nil)
        let request = URLRequest(url: url!)
        webViews.loadRequest(request)

    }
    
    
    //-- load Detail Book
    
    
    func loadDetailBook(){

            let lefMenu = self.storyboard?.instantiateViewController(withIdentifier: "LeftMenuReadBook") as! LeftMenuReadBook
            lefMenu.readBook = self
            lefMenu.totalChapter = self.bookChapters
            self.slideMenuController()?.changeLeftViewController(lefMenu, closeLeft: true)
            if self.bookChapters.count > 0 {
            self.chapters = self.bookChapters[0]
            self.setupUI(size: self.size, color: self.color, background: self.backGround, font_Family: self.font_Family,chapter :self.chapters!)
        }
            self.loading.stopAnimating()
        
    }
    

    func alertView(title : String , ok : String){
        
        let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension ReadBookVC {
    
    
    
    func sendDataBook(chapter: BookChapters) {
        self.chapters = chapter
        self.setupUI(size: size, color: color, background: backGround, font_Family: font_Family,chapter : chapter)
        
    }
    
    
    func setupUI(size : String, color: String, background : String, font_Family : String, chapter : BookChapters) {
        var bg : UIColor!
        if backGround == "white" {
            bg = UIColor.white
        }else{
            bg = UIColor(red: 67/255, green:78/255, blue:93/255, alpha:1.0)
        }
        let fontSize:String = "font-size:\(size); "
        let fontColor: String = "color:\(color); "
        let fontFamily : String = "font-family:\(font_Family); "
        if let text = chapter.content {
            let HTMLString = "<body style=\"margin-top: 50; padding:10\">" + "<div style=\"" + fontSize + fontColor + fontFamily + "\">" + text + "</div></body>"
            webViews.backgroundColor = bg
            webViews.loadHTMLString(HTMLString, baseURL: nil)
        }
        
    }

    

}


