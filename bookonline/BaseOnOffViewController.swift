//
//  BaseOnOffViewController.swift
//  bookonline
//
//  Created by Enrik on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class BaseOnOffViewController: BaseViewController {

    var fliterButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isTranslucent = false
        fliterButton = UIBarButtonItem(image: UIImage(named: "Fliter"), style: .plain, target: self, action: #selector(actionFliterButton))
        self.navigationItem.rightBarButtonItems?.append(fliterButton)
    }
    
    func actionFliterButton() {

    }

}
