//
//  OfflineVC.swift
//  bookonline
//
//  Created by ReasonAmu on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift

class OfflineVC: BaseViewController {

    let titleText = "Offline"

    @IBOutlet weak var offlineTableView: UITableView!
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var containerView: UIView!
    
    
    var offlineStories = [OfflineStory]()
    var stories = [OfflineStory]()
    
    let offlineTableCellIdentifier = "OfflineTableCell"
    
    var saveGenres: [String] = []
    var realm: Realm!
    
    var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        realm = try! Realm()
        
        self.title = titleText
        offlineTableView.delegate = self
        offlineTableView.dataSource = self
        
        offlineTableView.tableFooterView = UIView()
        self.offlineTableView.register(UINib(nibName: "AdsTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell1")

        let nib = UINib(nibName: "OfflineTableViewCell", bundle: nil)
        offlineTableView.register(nib, forCellReuseIdentifier: offlineTableCellIdentifier)

        addButton2(imageName: "Fliter")
        
        segmentControl.addTarget(self, action: #selector(changeSegment), for: .valueChanged)

        self.containerView.isHidden = true 
    }
    
    func changeSegment() {
        if self.segmentControl.selectedSegmentIndex == 0 {
            self.containerView.isHidden = true
        } else {
            self.containerView.isHidden = false 
        }
    }

    override func viewDidLayoutSubviews() {
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64))
    }
    
    override func actionBtn2() {
        
        let objects = realm.objects(OfflineStory.self)
        
        var genres: [String] = []
        
        if self.segmentControl.selectedSegmentIndex == 0 {
            
            for story in objects {
                if story.book_type == "0" {
                    let genre = story.category_name
                    var isDuplicated = false
                    for item in genres {
                        if item == genre {
                            isDuplicated = true
                            break
                        }
                    }
                    if isDuplicated == false {
                        genres.append(genre)
                    }
                }
                
            }
            
        } else {
            
            let audioOfflineVC = self.childViewControllers[0] as! AudioOfflineVC
            
            for story in objects {
                if story.book_type == "1" {
                    let genre = story.category_name
                    var isDuplicated = false
                    for item in genres {
                        if item == genre {
                            isDuplicated = true
                            break
                        }
                    }
                    if isDuplicated == false {
                        genres.append(genre)
                    }
                }
                
            }
            
        }

        let storyBoard = UIStoryboard(name: "Offline", bundle: nil)
        let fliterOfflineVC = storyBoard.instantiateViewController(withIdentifier: "OfflineFliterViewController") as! OfflineFliterViewController
        
        fliterOfflineVC.genres = genres
        fliterOfflineVC.offlineVC = self
        
        if self.segmentControl.selectedSegmentIndex == 0 {
            if self.saveGenres.count > 0 {
                fliterOfflineVC.savedGenres = self.saveGenres
            }
        } else {
            
            let audioOfflineVC = self.childViewControllers[0] as! AudioOfflineVC
            
            if audioOfflineVC.saveGenres.count > 0 {
                fliterOfflineVC.savedGenres = audioOfflineVC.saveGenres
            }
            
        }

        self.present(fliterOfflineVC, animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        requestData()
    }
    
    func requestData() {
        self.stories = []
        self.offlineStories = []
        let objects = realm.objects(OfflineStory.self)
        
        if self.saveGenres.count > 0 {
            
            for object in objects {
                
                if object.book_type == "0" {
                    var index = 0
                    while (index < saveGenres.count) && (saveGenres[index] != object.category_name) {
                        index += 1
                    }
                    if index < saveGenres.count {
                        offlineStories.append(object)
                    }
                }
                
            }
            
        } else {
            for object in objects {
                if object.book_type == "0" {
                    offlineStories.append(object)
                }
                
            }
        }
        self.stories = offlineStories
        self.offlineTableView.reloadData()
    }
    
    func deleteStory(_ offlineStory: OfflineStory, indexPath: IndexPath) {

        let cell = offlineTableView.cellForRow(at: indexPath) as! OfflineTableViewCell
        
        if cell.isFavorited == true {
            let objects = realm.objects(FavoriteBookOffline.self)
            
            for object in objects {
                if object.id == offlineStory.id {
                    try! realm.write {
                        realm.delete(object)
                    }
                    break
                }
            }
        }
        
        var sumSize = 0
        for chapter in offlineStory.chapters {
            sumSize = sumSize + chapter.content.utf8.count
        }
        var imageSize = 0
        if offlineStory.imageData != nil {
            imageSize = (offlineStory.imageData?.length)!
        }
        let size = UserDefaults.standard.value(forKey: k_Size) as! Int
        let newSize = size - sumSize - imageSize
        UserDefaults.standard.set(newSize, forKey: k_Size)

        try! realm.write {
            realm.delete(offlineStory)
        }
        
        
        
    }
    
    func convert(from offlineStory: OfflineStory) -> TopBook {
        let topBook = TopBook()
        
        topBook.id = offlineStory.id
        topBook.name = offlineStory.name
        topBook.image = offlineStory.image
        topBook.description = offlineStory.bookDescription
        topBook.book_source = offlineStory.book_source
        topBook.author = offlineStory.author
        topBook.country_id = offlineStory.country_id
        topBook.category_id = offlineStory.category_id
        topBook.page_number = offlineStory.page_number
        topBook.publisher = offlineStory.publisher
        topBook.publish_date = offlineStory.publish_date
        topBook.create_date = offlineStory.create_date
        topBook.last_update = offlineStory.last_update
        topBook.isbn = offlineStory.isbn
        topBook.language_type = offlineStory.language_type
        topBook.book_type = offlineStory.book_type
        topBook.chap_number = offlineStory.chap_number
        topBook.num_view = offlineStory.num_view
        topBook.category_name = offlineStory.category_name
        
        return topBook
    }
}

extension OfflineVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = stories.count + (stories.count/5)
        print(count)
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 5 == 0 && indexPath.row != 0{
            
            let cell = offlineTableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! AdsTableViewCell
            return cell
            
        }else{
            
            let indexPatchRow = indexPath.row - (indexPath.row/5)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: offlineTableCellIdentifier, for: indexPath) as! OfflineTableViewCell
            
            cell.configure(story: stories[indexPatchRow])
            
            return cell
        }}
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .destructive, title: "DELETE") { (action, index) in

        if indexPath.row % 5 == 0 && indexPath.row != 0{
            print("1234")
            

        }else{
            let indexPathRow = indexPath.row - (indexPath.row/5)
            let indexPath1 = NSIndexPath(row: indexPathRow, section: 0)
            self.removeDirectory(story: self.stories[indexPathRow])
            self.deleteStory(self.stories[indexPathRow], indexPath: indexPath1 as IndexPath)
            self.stories.remove(at: indexPathRow)
            self.requestData()
        }
        }
        if indexPath.row % 5 == 0 && indexPath.row != 0{
             return nil
        }else{
             return [edit]
            }
       
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row % 5 == 0 && indexPath.row != 0{
            return false
        }
        else{
            return true
        }
    }

    
    func removeDirectory(story: OfflineStory) {
        
        for chapter in story.chapters {
            if chapter.file_audio != "" {
                
                if let dir1 = kDOCUMENT_DIRECTORY_PATH {
                    let path = dir1 + chapter.file_audio
                    
                    let attributes = try! FileManager.default.attributesOfItem(atPath: path)
                    let fileSize = attributes[FileAttributeKey.size] as! Int
                    
                    let size = UserDefaults.standard.value(forKey: k_Size) as! Int
                    let newSize = size - fileSize
                    UserDefaults.standard.set(newSize, forKey: k_Size)
                }
                
            }
        }
        
        if let dir = kDOCUMENT_DIRECTORY_PATH {
                do {
                    let path = dir + "/\(story.name)"
                    try FileManager.default.removeItem(atPath: path)
                } catch {
                    print(error.localizedDescription)
                }
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row % 5 == 0 && indexPath.row != 0{
            UIApplication.shared.openURL(URL(string: "https://www.google.com")!)
        }else{
            let indexPathRow = indexPath.row - (indexPath.row/5)
            
            let storyboard = UIStoryboard(name: "Online", bundle: nil)
            let detailVC = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
            
            let offlineStory = self.stories[indexPathRow]
            
            var bookChapters: [BookChapters] = []
            
            for chapter in offlineStory.chapters {
                var bookChapter = BookChapters()
                bookChapter.chap_name = chapter.chap_name
                bookChapter.chap_no = chapter.chap_no
                bookChapter.content = chapter.content
                bookChapter.file_audio = chapter.file_audio
                
                bookChapters.append(bookChapter)
            }
            
            detailVC.isOffline = true
            detailVC.data = self.convert(from: offlineStory)
            detailVC.bookChapters = bookChapters
            detailVC.checkView = false
            detailVC.checkRecently = 1
            
            detailVC.isAudioBook = false 
            
            if let imageData = offlineStory.imageData {
                detailVC.imageStory = UIImage(data: imageData as Data)
            }
            
            self.navigationController?.pushViewController(detailVC, animated: true)
            self.endSearchBar()
        }
    }
}

// MARK: Search 
extension OfflineVC {
    
    override func actionSearch(_ sender: UIBarButtonItem) {
        super.actionSearch(sender)
        
        
    }
    
    override func endSearchBar() {
        super.endSearchBar()
        
        self.offlineTableView.tableHeaderView = UIView()
    }
    
    override func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        super.searchBarTextDidBeginEditing(searchBar)
        
        
    }
    
    override func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        super.searchBarCancelButtonClicked(searchBar)
        
        self.stories = []
        self.stories = self.offlineStories
        self.offlineTableView.reloadData()
        
        if segmentControl.selectedSegmentIndex == 1 {
            for viewController in self.childViewControllers {
                if let audioOfflineVC = viewController as? AudioOfflineVC {
                    audioOfflineVC.stories = []
                    audioOfflineVC.stories = audioOfflineVC.offlineStories
                    audioOfflineVC.audioTableView.reloadData()
                }
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if self.offlineTableView.tableHeaderView != headerView {
//            self.offlineTableView.tableHeaderView = headerView
//        }
        
        if searchBar.text != "" {
            if self.segmentControl.selectedSegmentIndex == 0 {
                self.stories = []
                for story in self.offlineStories {
                    if story.name.uppercased().contains(searchBar.text!.uppercased()) {
                        self.stories.append(story)
                    }
                }
                self.offlineTableView.reloadData()
           
            } else {
                for viewController in self.childViewControllers {
                    if let audioOfflineVC = viewController as? AudioOfflineVC {
                        audioOfflineVC.stories = []
                        for story in audioOfflineVC.offlineStories {
                            if story.name.uppercased().contains(searchBar.text!.uppercased()) {
                                audioOfflineVC.stories.append(story)
                            }
                        }
                        audioOfflineVC.audioTableView.reloadData()
                    }
                }

            }

        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.searchBar.isFirstResponder == true {
            endSearchBar()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("ok")
        endSearchBar()
    }
}

