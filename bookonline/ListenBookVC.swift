//
//  ListenBookVC.swift
//  bookonline
//
//  Created by Enrik on 1/21/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import AVFoundation

class ListenBookVC: UIViewController {

    @IBOutlet weak var imageSong: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var labelCurrentTime: UILabel!
    
    @IBOutlet weak var labelRemainingTime: UILabel!
    
    @IBOutlet weak var buttonRepeat: UIButton!
    
    @IBOutlet weak var buttonPlay: UIButton!
    
    @IBOutlet weak var buttonNext: UIButton!
    
    @IBOutlet weak var buttonPrevious: UIButton!
    
    @IBOutlet weak var topConstraintSlider: NSLayoutConstraint!
    
    
    @IBOutlet weak var slider: UISlider!
    
    let baseURL = "http://bookonline.myaawu.com/"
    
    var isOffline: Bool = false
    
    var audioPlayer = AudioPlayer.shared
    
    var listChapters = [BookChapters]()
    
    var topBook: TopBook!
    
    var imageStory: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        audioPlayer.listSong = listChapters
        
        if self.isOffline == false {
            if topBook.image != nil {
                if let url = URL(string: baseURL + topBook.image!) {
                    imageSong.kf.setImage(with: url)
                }
            }
        } else {
            if self.imageStory != nil {
                imageSong.image = self.imageStory
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidPlayToEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(initPlayView), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.isOffline == false {
            if topBook.image != nil {
                if let url = URL(string: baseURL + topBook.image!) {
                    imageSong.kf.setImage(with: url)
                }
            }
        } else {
            if self.imageStory != nil {
                imageSong.image = self.imageStory
            }
        }
        
        self.topConstraintSlider.constant = -4
        
        self.slider.setThumbImage(UIImage(named: "img-slider-thumb"), for: .normal)
        
        if audioPlayer.playing == false {
            self.buttonPlay.setImage(UIImage(named: "img-player-play"), for: .normal)
        }
        
        switch audioPlayer.repeating {
        case 0:
            self.buttonRepeat.setImage(UIImage(named: "img-player-repeat-none"), for: .normal)
        case 1:
            self.buttonRepeat.setImage(UIImage(named: "img-player-repeat-1"), for: .normal)
        case 2:
            self.buttonRepeat.setImage(UIImage(named: "img-player-repeat"), for: .normal)
        default:
            break
        }

        if audioPlayer.song != nil {
            if audioPlayer.song.id == self.topBook.id {
                self.initPlayView()
            }
        }
        
    }

    func moveCellTable() {
        let index = audioPlayer.songPosition
        let indexPath = IndexPath(row: index!, section: 0)
        self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
    }
    
    func playerItemDidPlayToEnd(_ notification: Notification){
        if audioPlayer.repeating == 0 {
            self.buttonPlay.setImage(UIImage(named: "img-player-play"), for: .normal)
        } else {
            self.initPlayView()
        }
    }
    
    func initPlayView() {

        self.tableView.reloadData()
        
        self.moveCellTable()
        
        self.buttonPlay.setImage(UIImage(named: "img-player-pause"), for: .normal)
        
        self.labelCurrentTime.text = "0:00"
        self.labelRemainingTime.text = "-0:00"
        self.slider.value = 0
        self.updateTime()
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
    }
    
    
    func updateTime(){
        
        if audioPlayer.playing {
            self.buttonPlay.setImage(UIImage(named: "img-player-pause"), for: .normal)
        } else {
            self.buttonPlay.setImage(UIImage(named: "img-player-play"), for: .normal)
        }
        
        let duration = CMTimeGetSeconds(AudioPlayer.shared.playerItem.duration)
        
        let currentTime = Float(CMTimeGetSeconds(self.audioPlayer.player.currentTime()))
        
        self.slider.value =  currentTime/Float(duration)
        
        if duration > 0 {
            let currentMinute = Int(currentTime / 60)
            let currentSecond = Int(round(currentTime - Float(currentMinute * 60)))
            self.labelCurrentTime.text = String(format:"%d",currentMinute) + ":" + String(format:"%02d",currentSecond)
            
            let remainingTime = Float(duration) - currentTime
            
            let remainingMinute = Int(remainingTime / 60)
            let remainingSecond = Int(round(remainingTime - Float(remainingMinute * 60)))
            self.labelRemainingTime.text = String(format: "- %d", remainingMinute) + ":" + String(format: "%02d", remainingSecond)
        }
        

    }

    
    @IBAction func actionRepeat(_ sender: Any) {
        var number: Int!
        switch audioPlayer.repeating {
        case Constant.RepeatNone:
            number = Constant.RepeatOne
            self.buttonRepeat.setImage(UIImage(named: "img-player-repeat-1"), for: .normal)
        case Constant.RepeatOne:
            number = Constant.RepeatAll
            self.buttonRepeat.setImage(UIImage(named: "img-player-repeat"), for: .normal)
        case Constant.RepeatAll:
            number = Constant.RepeatNone
            self.buttonRepeat.setImage(UIImage(named: "img-player-repeat-none"), for: .normal)
        default:
            break
        }
        audioPlayer.actionRepeatSong(repeating: number)
    }
    
    @IBAction func actionSlider(_ sender: Any) {
        audioPlayer.actionSliderDuration((sender as! UISlider).value)
    }
    
    @IBAction func actionPlay(_ sender: Any) {
        
        if audioPlayer.player == nil {
            audioPlayer.songPosition = 0
            audioPlayer.setup()
            self.initPlayView()
        } else {
            audioPlayer.actionPlayPause()
        }

        if audioPlayer.playing  {
            self.buttonPlay.setImage(UIImage(named: "img-player-pause"), for: .normal)
        } else {
            self.buttonPlay.setImage(UIImage(named: "img-player-play"), for: .normal)
        }
    }

    @IBAction func actionNext(_ sender: Any) {
        audioPlayer.actionNextSong()
        initPlayView()
        
    }
    
    @IBAction func actionPrevious(_ sender: Any) {
        audioPlayer.actionPreviousSong()
        initPlayView()
        
    }
    

    @IBAction func actionDismissView(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

}

extension ListenBookVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listChapters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as UITableViewCell!
        
        if cell == nil {
           cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "Cell")
        }
        
        cell!.textLabel?.text = listChapters[indexPath.row].chap_name
        cell?.backgroundColor = UIColor.darkGray
        cell?.textLabel?.textColor = UIColor.white
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let chapter = listChapters[indexPath.row]
        
        audioPlayer.songPosition = indexPath.row
        audioPlayer.setup()
        self.initPlayView()
    }
}
