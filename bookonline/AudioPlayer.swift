//
//  PlayMusic.swift
//  FreeMusic
//
//  Created by Enrik on 12/3/16.
//  Copyright © 2016 Enrik. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import MediaPlayer
import Kingfisher

let baseUrl = "http://bookonline.myaawu.com"

let kDOCUMENT_DIRECTORY_PATH = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first

class AudioPlayer {
    static let shared = AudioPlayer()
    
    var player: AVPlayer!
    var playerItem: AVPlayerItem!
    
    var title = ""
    var artist = ""
    
    var duration: Float!
    var currentTiem: Float!
    
    var repeating : Int = Constant.RepeatAll
    var playing = false
    
    var shuffle: Bool = false
    
    var songPosition: Int!
    var song: BookChapters!
    var listSong: [BookChapters]!
    
    var isLinkLoaded = false
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func playLink(url: String) {
        
        self.initRemoteControl()
        
        if self.player != nil {
            self.player.pause()
        }
        
        var playUrl = URL(fileURLWithPath: "")
        
        if let checkURL = URL(string: baseUrl + url) {
            playUrl = checkURL
        } else {
            let dir = kDOCUMENT_DIRECTORY_PATH!
            playUrl = URL(fileURLWithPath: dir + url)
            print(playUrl)
        }
        
        print(kDOCUMENT_DIRECTORY_PATH!)
        
        
        playerItem = AVPlayerItem(url: playUrl)
        player = AVPlayer(playerItem: playerItem)
        player.rate = 1.0
        player.play()

        playing = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidPlayToEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
    }
    
    @objc func playerItemDidPlayToEnd(_ notification: Notification){
        if self.repeating == 0 {
            self.playing = false
        } else if self.repeating == 1 {
            self.player.seek(to: kCMTimeZero)
            self.player.play()
        } else if self.repeating == 2 {
            if self.isLinkLoaded == true {
                
                self.actionNextSong()
                
                self.isLinkLoaded = false
            }
            
        }
    }
    
    func setup() {
        
        if self.player != nil {
            self.player.pause()
        }
        self.song = self.listSong[songPosition]
        

            let link = self.song.file_audio!
            
            self.playLink(url: link)
            
            self.setupInfoCenter()
            
            self.isLinkLoaded = true

    }
    
    
    // MARK: Action

    func actionRepeatSong(repeating: Int!){
        self.repeating = repeating
    }
    
    func actionShuffle(_ shuffle: Bool){
        self.shuffle = shuffle
    }
    
    @objc func actionPlayPause() {
        if self.playing == true {
            self.player.pause()
            self.playing = false
        } else {
            self.player.play()
            self.playing = true
        }
        if MPNowPlayingInfoCenter.default().nowPlayingInfo != nil {
            MPNowPlayingInfoCenter.default().nowPlayingInfo![MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber(value: CMTimeGetSeconds((player.currentTime())))
        }
        
    }
    
    func actionSliderDuration(_ value: Float) {
        duration = Float(CMTimeGetSeconds(playerItem.duration))
        let timeToSeek = value * duration
        let time = CMTimeMake(Int64(timeToSeek), 1)
        self.player.seek(to: time)
    }
    
    @objc func actionNextSong() {
        
            if songPosition < listSong.count - 1 {
                songPosition = songPosition + 1
            } else {
                songPosition = 0
            }
            self.setup()

    }
    
    @objc func actionPreviousSong() {

            if songPosition > 0 {
                songPosition = songPosition - 1
            } else {
                songPosition = listSong.count - 1
            }
            self.setup()
        
    }
    
    // MARK: Remote Control Center
    func setupInfoCenter() {

        if (song.image != nil) && (song.image != "") {
            let imageLink = baseUrl + song.image!
            let url = URL(string: imageLink)
            KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            let artWork = MPMediaItemArtwork(image: image!)
            self.setInfoCenterWithImage(image: artWork)
            })
        }
        
        

    }
    
    func setInfoCenterWithImage(image: MPMediaItemArtwork) {
        let item = AudioPlayer.shared.playerItem
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [
            MPMediaItemPropertyArtist: song.author!,
            MPMediaItemPropertyTitle: song.name!,
            MPMediaItemPropertyArtwork: image,
            MPMediaItemPropertyPlaybackDuration: NSNumber(value: CMTimeGetSeconds((item?.asset.duration)!)),
            MPNowPlayingInfoPropertyPlaybackRate: NSNumber(value: 1)
        ]
    }
    
    func initRemoteControl() {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        //self.becomeFirstResponder()

        do  {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
            } catch {
                print(error.localizedDescription)
            }
        } catch {
            print(error.localizedDescription)
        }
        
    }

}


