//
//  TopBook.swift
//  bookonline
//
//  Created by ReasonAmu on 1/12/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import Foundation
import SwiftyJSON

class TopBook : BaseParseJson {
    
    var id : String?
    var name : String?
    var image : String?
    var description : String?
    var book_source: String?
    var author : String?
    var country_id : String?
    var category_id : String?
    var page_number : String?
    var publisher : String?
    var publish_date : String?
    var create_date : String?
    var last_update : String?
    var isbn : String?
    var language_type : String?
    var book_type : String?
    var chap_number : String?
    var num_view : String?
    var category_name: String?
    var last_time: Int?
    
    override func parse(from json: JSON) {
        super.parse(from: json)

        if let id : String = json["id"].string {
            self.id = id
        }
        
        if let name : String = json["name"].string {
            self.name = name
        }
        
        if let image : String = json["image"].string {
            self.image = image
        }
        
        if let description : String = json["description"].string {
            self.description = description
        }
        
        if let book_source : String = json["book_source"].string {
            self.book_source = book_source
        }
        
        if let author : String = json["author"].string {
            self.author = author
        }
        
        if let country_id : String = json["country_id"].string {
            self.country_id = country_id
        }
        
        if let category_id : String = json["category_id"].string {
            self.category_id = category_id
        }
        
        if let page_number : String = json["page_number"].string {
            self.page_number = page_number
        }
        
        if let publisher : String = json["publisher"].string {
            self.publisher = publisher
        }
        
        if let publish_date : String = json["publish_date"].string {
            self.publish_date = publish_date
        }
        
        if let last_update : String = json["last_update"].string {
            self.last_update = last_update
        }
        
        if let isbn : String = json["isbn"].string {
            self.isbn = isbn
        }
        
        if let publisher : String = json["publisher"].string {
            self.publisher = publisher
        }
        
        if let language_type : String = json["language_type"].string {
            self.language_type = language_type
        }
        
        if let book_type : String = json["book_type"].string {
            self.book_type = book_type
        }
        
        if let chap_number : String = json["chap_number"].string {
            self.chap_number = chap_number
        }
        
        if let num_view : String = json["num_view"].string {
            self.num_view = num_view
        }
        
        if let category_name : String =  json["category_name"].string {
            self.category_name = category_name
        }
        
    }
}
