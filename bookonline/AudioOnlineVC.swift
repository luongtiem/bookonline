//
//  AudioOnlineVC.swift
//  bookonline
//
//  Created by Enrik on 2/13/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class AudioOnlineVC: BaseVC {

    
    @IBOutlet weak var audioCollectionView: UICollectionView!
    
    let cellID = "Cell"
    
    var books : [TopBook] = []
    var fromBook : Int = 10
    var stories: [TopBook] = []
    var showStories: [TopBook] = []
    
    var saveGenres: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        audioCollectionView.delegate = self
        audioCollectionView.dataSource = self
              audioCollectionView.register(UINib(nibName: "AdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell1")
        audioCollectionView.register(UINib(nibName: "FavoriteOnlineCell", bundle: nil), forCellWithReuseIdentifier: cellID)
        audioCollectionView.contentInset = UIEdgeInsetsMake(6, 6, 6, 6)
        audioCollectionView.backgroundColor = UIColor(red: 67/255, green: 78/255, blue: 93/255, alpha: 1.0)
        audioCollectionView.addSubview(refreshControl)
        
        loadData(number_book: 10, from: 0, book_type: .audio) { (result) in
            self.books += result
            self.stories.removeAll()
            self.stories = self.books
            self.showStories = self.stories
            self.audioCollectionView.reloadData()
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if saveGenres.count > 0 {
            self.stories = []
            for book in self.books {
                if saveGenres.contains(book.category_name!) {
                    self.stories.append(book)
                }
            }
            self.showStories = self.stories
            self.audioCollectionView.reloadData()
        } else {
            self.stories.removeAll()
            self.stories = self.books
            self.showStories = self.stories
            self.audioCollectionView.reloadData()
        }
    }

    func loadData(number_book : Int, from : Int , book_type : BookType, succces : @escaping ([TopBook]) -> () ){
        var resultBooks : [TopBook] = []
        
        
        ManagerData.instance.getTopBook(number_book: number_book, from: from, book_type: book_type, success: { json, msg in
            
            for item in (json?.array)! {
                resultBooks.append(TopBook(from: item))
                self.audioCollectionView.reloadData()
                
            }
            self.fromBook += 10
            succces(resultBooks)
            
        }) { (msg) in
            print(msg)
        }

    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if refreshControl.isRefreshing == true {
            self.books.removeAll()
            self.stories.removeAll()
            loadData(number_book: 10, from: 0, book_type: .audio) { (result) in
                print(result)
                self.books = result
                self.stories = self.books
                self.showStories = self.stories
                self.audioCollectionView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }

}
extension AudioOnlineVC : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return showStories.count + (stories.count/6)

    }
    
    //--
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row % 6 == 0 && indexPath.row != 0{
            let width = collectionView.frame.width/3 - 10
            return CGSize(width: collectionView.frame.width, height: width)
            
        }else{
            let width = collectionView.frame.width/3 - 10
            
            return CGSize(width: width, height: width*2)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3.0
    }
    
    
    //-- select
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row % 6 == 0 && indexPath.row != 0{
            print("chay quang cao")
        }else{
            let indexPatchRow = indexPath.row - (indexPath.row/6)
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        detail.data = showStories[indexPatchRow]
        
        detail.isAudioBook = true 
        
        navigationController?.pushViewController(detail, animated: true)
        
        let parent = self.parent as! OnlineVC
        parent.endSearchBar()
    }
    }
}

extension AudioOnlineVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row % 6 == 0 && indexPath.row != 0{
            print("1")
        }else{
            let indexPatchRow = indexPath.row - (indexPath.row/6)

        if let stringURL = showStories[indexPatchRow].image {
            let url = URL(string: "\(baseURL)\(stringURL)")
            (cell as! FavoriteOnlineCell).imageBook.kf.setImage(with: url, options: [.transition(.fade(0.5))])
        }
        
        (cell as! FavoriteOnlineCell).books = showStories[indexPatchRow]
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row % 6 == 0 && indexPath.row != 0{
            print("1")
        }else{
        (cell as! FavoriteOnlineCell).imageBook.kf.cancelDownloadTask()
        }}
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row % 6 == 0 && indexPath.row != 0{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell1", for: indexPath)  as! AdsCollectionViewCell
        return cell
        }else{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)  as! FavoriteOnlineCell
        cell.imageBook.kf.indicatorType = .activity
        return cell
    }
    }
}
