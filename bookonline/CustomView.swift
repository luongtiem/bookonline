//
//  CustomView.swift
//  Copie
//
//  Created by Enrik on 11/10/16.
//  Copyright © 2016 Apecsoft.Asia. All rights reserved.
//

import Foundation
import UIKit

class CustomView: UIView {
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let uiview = super.hitTest(point, with: event)
        print(uiview)
        return overlapHitTest(point, with: event)
    }
    
}

extension UIView {
    
    func overlapHitTest(_ point: CGPoint, with event: UIEvent?, invisibleOn: Bool = false) -> UIView? {
        // 1
        let invisible = (isHidden || alpha == 0) && invisibleOn
        
        if !isUserInteractionEnabled || invisible {
            return nil
        }
        //2
        var hitView: UIView? = self
        if !self.point(inside: point, with: event) {
            if clipsToBounds {
                return nil
            } else {
                hitView = nil
            }
        }
        //3
        for subview in subviews.reversed() {
            let insideSubview = convert(point, to: subview)
            if let sview = subview.overlapHitTest(insideSubview, with: event) {
                return sview
            }
        }
        return hitView
    }
}
