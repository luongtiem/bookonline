//
//  BookChapters.swift
//  bookonline
//
//  Created by ReasonAmu on 1/12/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import Foundation
import SwiftyJSON

class BookChapters : TopBook {

    var content : String?
    var chap_name : String?
    var chap_no : String?
    var file_audio  : String?
    
    
    
    override func parse(from json: JSON) {
        
        super.parse(from: json)
        
        if let content : String = json["content"].string {
            self.content = content
        }
        
        if let chap_name : String = json["chap_name"].string {
            self.chap_name = chap_name
        }
        
        if let chap_no : String = json["chap_no"].string {
            self.chap_no = chap_no
        }
        
        if let file_audio : String = json["file_audio"].string {
            self.file_audio = file_audio
        }
        
    }
}
