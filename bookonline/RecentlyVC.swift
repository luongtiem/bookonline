//
//  RecentlyVC.swift
//  bookonline
//
//  Created by ReasonAmu on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class RecentlyVC: BaseViewController {
    @IBOutlet var segmentedRecently: UISegmentedControl!

    @IBOutlet var recentlyOnline: UIView!
    @IBOutlet var recentlyOffline: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        recentlyOffline.isHidden = true
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        addButton2(imageName: "Pen")
    }
    override func actionBtn2() {
        if segmentedRecently.selectedSegmentIndex == 0 {
            let storyboard = UIStoryboard(name: "Favorite", bundle: nil)
            let favoritefix = storyboard.instantiateViewController(withIdentifier: "FavoriteFixVC") as! FavoriteFix
            favoritefix.checkView = 0
            self.present(favoritefix, animated: true, completion: nil)
        }else{
            let storyboard = UIStoryboard(name: "Favorite", bundle: nil)
            let favoritefix = storyboard.instantiateViewController(withIdentifier: "FavoriteFixVC") as! FavoriteFix
            favoritefix.checkView = 3
            self.present(favoritefix, animated: true, completion: nil)
        }
    }
//    @IBAction func showViewFixRecently(_ sender: Any) {
//        let storyboard = UIStoryboard(name: "Favorite", bundle: nil)
//        let favoritefix = storyboard.instantiateViewController(withIdentifier: "FavoriteFixVC")
//        self.present(favoritefix, animated: true, completion: nil)
//    }

    @IBAction func actionChangeRecentlyView(_ sender: Any) {
        if  segmentedRecently.selectedSegmentIndex == 0 {
            recentlyOnline.isHidden = false
            recentlyOffline.isHidden = true
        }
        else{
            recentlyOnline.isHidden = true
            recentlyOffline.isHidden = false
            
        }
    }

}
extension RecentlyVC {
    
    override func actionSearch(_ sender: UIBarButtonItem) {
        super.actionSearch(sender)
        
        
    }
    
    override func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        super.searchBarTextDidBeginEditing(searchBar)
        
        
    }
    
    override func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        super.searchBarCancelButtonClicked(searchBar)
        if segmentedRecently.selectedSegmentIndex == 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "searchBarRecentlyCancel"), object: nil)
        }
        else{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "searchBarRecentlyOfflineCancel"), object: nil)
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if segmentedRecently.selectedSegmentIndex == 0{
            if searchBar.text != "" {
                let textInput:[String : String] = ["text": searchBar.text!]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "searchRecentlyDidChange"), object: nil, userInfo: textInput)
            }}
        else{
            let textInput:[String : String] = ["text": searchBar.text!]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "searchRecentlyOfflineDidChange"), object: nil, userInfo: textInput)
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.searchBar.isFirstResponder == true {
            endSearchBar()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        endSearchBar()
    }
}
