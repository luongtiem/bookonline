//
//  BaseVC.swift
//  bookonline
//
//  Created by ReasonAmu on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    
    let baseURL = "http://bookonline.myaawu.com/"
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRefreshControl()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupRefreshControl(){
        let time = Date()

        refreshControl.tintColor = UIColor.white
        let attr = [NSForegroundColorAttributeName:UIColor.white]
        refreshControl.attributedTitle  = NSAttributedString(string: "\(R.string.bookonline.update()) \(convertDateToString(date: time))", attributes: attr)
    }
    
    func convertDateToString(date : Date) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"///this is you want to convert format
        dateFormatter.locale = Locale.current
        let timeStamp = dateFormatter.string(from: date)
        return timeStamp
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
