//
//  CellDetail.swift
//  bookonline
//
//  Created by ReasonAmu on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift
import ReachabilitySwift


class CellDetail: UITableViewCell {
    var liked = false
    var liked1 = false
    @IBOutlet weak var webViews: UIWebView!
    @IBOutlet var bannerAds: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var btnReadBook: CustomButton!
    @IBOutlet weak var btnListenBook: CustomButton!
    
    
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var subTitle2: UILabel!
    
    @IBOutlet weak var title3: UILabel!
    @IBOutlet weak var content3: UILabel!
    @IBOutlet weak var translate3: UILabel!
    @IBOutlet weak var subTitle3: UILabel!

    @IBOutlet weak var title4: UILabel!
    @IBOutlet weak var imageView4: UIImageView!
    
    var detailVC: DetailVC?
    var realm: Realm!
    var data : TopBook!
    var checkRecently = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        realm = try! Realm()
        btnReadBook.addTarget(self, action: #selector(handleReadBook), for: .touchUpInside)
        btnListenBook.addTarget(self, action: #selector(handleListenBook), for: .touchUpInside)
        setupUI()
        
        btnListenBook.isHidden = true
        
        let tabGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(bannerTapped(tapGestureRecognizer:)))
        bannerAds.isUserInteractionEnabled = true
        bannerAds.addGestureRecognizer(tabGestureRecognizer)
    }
    func bannerTapped(tapGestureRecognizer: UITapGestureRecognizer){
        UIApplication.shared.openURL(URL(string: "https://www.google.com")!)
    }
    func checkRead(){
        realm = try? Realm()
        let objects = realm.objects(RecentlyBook.self)
        liked = false
        for object in objects{
            if data.id == object.id
            {
                liked = true
                break
            }
        }
        let objectsOff = realm.objects(RecentlyBookOffline.self)
        liked = false
        for object in objectsOff{
            if data.id == object.id
            {
                liked = true
                break
            }
        }
    }
    
    func setupUI(){
        
        title.text = R.string.bookonline.sourceBook()
        title2.text = R.string.bookonline.category()
        title3.text = R.string.bookonline.about()
        title4.text = R.string.bookonline.advertisement()
        self.backgroundColor = detailVC?.colorBackground
    }
    
    var setupDetail : ModelDetailBook?{
        
        didSet{
            subtitle.text = setupDetail?.sourceBook
            subTitle2.text = setupDetail?.category
            webViews.isOpaque = false
            webViews.backgroundColor = detailVC?.colorBackground
            
            if let descriptions = setupDetail?.contentBook{
                let HTML = "<font color = #DFE0E4>" + descriptions + "</font>"
                webViews.loadHTMLString(HTML, baseURL: nil)
            }
            
            
    
        }
    }
    
  

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension CellDetail {
    
    func handleReadBook(){
        
        if checkRecently{
            let reachability = Reachability()!
            
            reachability.whenReachable = { reachability in
                DispatchQueue.main.async {
                    if reachability.isReachableViaWiFi {
                        self.detailVC?.presentReadBook()
                        if !self.liked {
                            let recentlyBook = RecentlyBook()
                            if let author = self.data.author {
                                recentlyBook.author = author
                            }
                            if let id = self.data.id{
                                recentlyBook.id = id
                            }
                            if let image = self.data.image{
                                recentlyBook.image = image
                            }
                            if let name = self.data.name{
                                recentlyBook.name = name
                            }
                            if let descriptionBook = self.data.description{
                                recentlyBook.descriptionBook = descriptionBook
                            }
                            if let book_source = self.data.book_source{
                                recentlyBook.book_source = book_source
                            }
                            if let country_id = self.data.country_id{
                                recentlyBook.country_id = country_id
                            }
                            if let category_id = self.data.category_id{
                                recentlyBook.category_id = category_id
                            }
                            if let page_number = self.data.page_number{
                                recentlyBook.page_number = page_number
                            }
                            if let publisher = self.data.publisher{
                                recentlyBook.publisher = publisher
                            }
                            if let publish_date = self.data.publish_date{
                                recentlyBook.publish_date = publish_date
                            }
                            if let create_date = self.data.create_date{
                                recentlyBook.create_date = create_date
                            }
                            if let last_update = self.data.last_update{
                                recentlyBook.last_update = last_update
                            }
                            if let isbn = self.data.isbn{
                                recentlyBook.isbn = isbn
                            }
                            if let language_type = self.data.language_type{
                                recentlyBook.language_type = language_type
                            }
                            
                            if let book_type = self.data.book_type{
                                recentlyBook.book_type = book_type
                            }
                            
                            if let chap_number = self.data.chap_number{
                                recentlyBook.chap_number = chap_number
                            }
                            if let num_view = self.data.num_view{
                                recentlyBook.num_view = num_view
                            }
                            if let category_name = self.data.category_name{
                                recentlyBook.category_name = category_name
                            }
                            recentlyBook.last_time = Int(Date().timeIntervalSince1970)
                            let objects = self.realm.objects(RecentlyBook.self)
                            var old = false
                            for obj in objects {
                                if obj.id == recentlyBook.id {
                                    old = true
                                    try! self.realm.write {
                                        obj.last_time = Int(Date().timeIntervalSince1970)
                                    }
                                }
                            }
                            if !old {
                                try! self.realm.write {
                                    self.realm.add(recentlyBook)
                                    self.liked = true
                                }
                            }
                        }
                    } else {
                        self.detailVC?.presentReadBook()
                        if !self.liked {
                            let recentlyBook = RecentlyBook()
                            if let author = self.data.author {
                                recentlyBook.author = author
                            }
                            if let id = self.data.id{
                                recentlyBook.id = id
                            }
                            if let image = self.data.image{
                                recentlyBook.image = image
                            }
                            if let name = self.data.name{
                                recentlyBook.name = name
                            }
                            if let descriptionBook = self.data.description{
                                recentlyBook.descriptionBook = descriptionBook
                            }
                            if let book_source = self.data.book_source{
                                recentlyBook.book_source = book_source
                            }
                            if let country_id = self.data.country_id{
                                recentlyBook.country_id = country_id
                            }
                            if let category_id = self.data.category_id{
                                recentlyBook.category_id = category_id
                            }
                            if let page_number = self.data.page_number{
                                recentlyBook.page_number = page_number
                            }
                            if let publisher = self.data.publisher{
                                recentlyBook.publisher = publisher
                            }
                            if let publish_date = self.data.publish_date{
                                recentlyBook.publish_date = publish_date
                            }
                            if let create_date = self.data.create_date{
                                recentlyBook.create_date = create_date
                            }
                            if let last_update = self.data.last_update{
                                recentlyBook.last_update = last_update
                            }
                            if let isbn = self.data.isbn{
                                recentlyBook.isbn = isbn
                            }
                            if let language_type = self.data.language_type{
                                recentlyBook.language_type = language_type
                            }
                            
                            if let book_type = self.data.book_type{
                                recentlyBook.book_type = book_type
                            }
                            
                            if let chap_number = self.data.chap_number{
                                recentlyBook.chap_number = chap_number
                            }
                            if let num_view = self.data.num_view{
                                recentlyBook.num_view = num_view
                            }
                            if let category_name = self.data.category_name{
                                recentlyBook.category_name = category_name
                            }
                            recentlyBook.last_time = Int(Date().timeIntervalSince1970)
                            let objects = self.realm.objects(RecentlyBook.self)
                            var old = false
                            for obj in objects {
                                if obj.id == recentlyBook.id {
                                    old = true
                                    try! self.realm.write {
                                        obj.last_time = Int(Date().timeIntervalSince1970)
                                    }
                                }
                            }
                            if !old {
                                try! self.realm.write {
                                    self.realm.add(recentlyBook)
                                    self.liked = true
                                }
                            }
                        }
                        
                    }
                }
            }
            reachability.whenUnreachable = { reachability in
                DispatchQueue.main.async{
                    let alert = UIAlertController(title:R.string.bookonline.noInternet(), message: R.string.bookonline.needInternetToConnect(), preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: R.string.bookonline.cancel(), style: UIAlertActionStyle.default, handler:nil))
                    self.detailVC?.present(alert, animated: true, completion: nil)
                }
            }
            
            do {
                try reachability.startNotifier()
            } catch {
                print("Unable to start notifier")
            }
            
            
        }else{
            self.detailVC?.presentReadBook()
            if !liked1 {
                let recentlyBookOffline = RecentlyBookOffline()
                if let author = data.author {
                    recentlyBookOffline.author = author
                }
                if let id = data.id{
                    recentlyBookOffline.id = id
                }
                if let image = data.image{
                    recentlyBookOffline.image = image
                }
                if let name = data.name{
                    recentlyBookOffline.name = name
                }
                if let descriptionBook = data.description{
                    recentlyBookOffline.descriptionBook = descriptionBook
                }
                if let book_source = data.book_source{
                    recentlyBookOffline.book_source = book_source
                }
                if let country_id = data.country_id{
                    recentlyBookOffline.country_id = country_id
                }
                if let category_id = data.category_id{
                    recentlyBookOffline.category_id = category_id
                }
                if let page_number = data.page_number{
                    recentlyBookOffline.page_number = page_number
                }
                if let publisher = data.publisher{
                    recentlyBookOffline.publisher = publisher
                }
                if let publish_date = data.publish_date{
                    recentlyBookOffline.publish_date = publish_date
                }
                if let create_date = data.create_date{
                    recentlyBookOffline.create_date = create_date
                }
                if let last_update = data.last_update{
                    recentlyBookOffline.last_update = last_update
                }
                if let isbn = data.isbn{
                    recentlyBookOffline.isbn = isbn
                }
                if let language_type = data.language_type{
                    recentlyBookOffline.language_type = language_type
                }
                
                if let book_type = data.book_type{
                    recentlyBookOffline.book_type = book_type
                }
                
                if let chap_number = data.chap_number{
                    recentlyBookOffline.chap_number = chap_number
                }
                if let num_view = data.num_view{
                    recentlyBookOffline.num_view = num_view
                }
                if let category_name = data.category_name{
                    recentlyBookOffline.category_name = category_name
                }
                recentlyBookOffline.last_time = Int(Date().timeIntervalSince1970)
                let objects = realm.objects(RecentlyBookOffline.self)
                var old = false
                for obj in objects {
                    if obj.id == recentlyBookOffline.id {
                        old = true
                        try! realm.write {
                            obj.last_time = Int(Date().timeIntervalSince1970)
                        }
                    }
                }
                if !old {
                    try! realm.write {
                        realm.add(recentlyBookOffline)
                        liked1 = true
                    }
                }
                
            }
            
        }
        
    }
    
    func handleListenBook() {
        detailVC?.presentListenBook()
    }
    
}
