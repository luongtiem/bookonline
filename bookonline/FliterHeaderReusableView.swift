//
//  FliterHeaderReusableView.swift
//  bookonline
//
//  Created by Enrik on 1/11/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class FliterHeaderReusableView: UICollectionReusableView {

    @IBOutlet weak var imageViewHeader: UIImageView!
    
    @IBOutlet weak var labelHeaderTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
