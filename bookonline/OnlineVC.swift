//
//  OnlineVC.swift
//  bookonline
//
//  Created by ReasonAmu on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import Kingfisher

class OnlineVC: BaseViewController {
    
    let cellID = "Cell"
    @IBOutlet var collectionView: UICollectionView!
    
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    var indexpathtest = 0
    var dataDetailBook : [BookChapters] = []
    var books : [TopBook] = []
    var dataCategory : [Categories] = []
    var fromBook : Int = 10
    var stories: [TopBook] = []
    var showStories: [TopBook] = []
    
    var saveGenres: [String] = []
    
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "FavoriteOnlineCell", bundle: nil), forCellWithReuseIdentifier: cellID)
        collectionView.register(UINib(nibName: "AdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell1")
        collectionView.contentInset = UIEdgeInsetsMake(6, 6, 6, 6)
        collectionView.backgroundColor = UIColor(red: 67/255, green:78/255, blue:93/255, alpha:1.0)
        collectionView.addSubview(refreshControl)
        
        
        loadData(number_book: 10, from: 0, book_type: .text) { (result) in
            self.books += result
            self.stories.removeAll()
            self.stories = self.books
            self.showStories = self.stories
            self.collectionView.reloadData()
            
        }
        
        addButton2(imageName: "Fliter")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if saveGenres.count > 0 {
            self.stories = []
            for book in self.books {
                if saveGenres.contains(book.category_name!) {
                    self.stories.append(book)
                }
            }
            self.showStories = self.stories
            self.collectionView.reloadData()
        } else {
            self.stories.removeAll()
            self.stories = self.books
            self.showStories = self.stories
            self.collectionView.reloadData()
        }
    }
    
    override func actionBtn2() {

        var genres: [String] = []
        if self.segmentControl.selectedSegmentIndex == 0 {
            for story in books {
                let genre = story.category_name
                var isDuplicated = false
                for item in genres {
                    if item == genre {
                        isDuplicated = true
                        break
                    }
                }
                if isDuplicated == false {
                    genres.append(genre!)
                }
            }
        } else {
            
            for viewController in self.childViewControllers {
                if let audioOnlineVC = viewController as? AudioOnlineVC {
                    for story in audioOnlineVC.books {
                        let genre = story.category_name
                        var isDuplicated = false
                        for item in genres {
                            if item == genre {
                                isDuplicated = true
                                break
                            }
                        }
                        if isDuplicated == false {
                            genres.append(genre!)
                        }
                    }
                }
            }
            
            
        }

        let storyBoard = UIStoryboard(name: "Offline", bundle: nil)
        let fliterOfflineVC = storyBoard.instantiateViewController(withIdentifier: "OfflineFliterViewController") as! OfflineFliterViewController
        
        fliterOfflineVC.genres = genres
        fliterOfflineVC.onlineVC = self
        fliterOfflineVC.isOffline = false
        
        if self.segmentControl.selectedSegmentIndex == 0 {
            if self.saveGenres.count > 0 {
                fliterOfflineVC.savedGenres = self.saveGenres
            }
        } else {
            for viewController in self.childViewControllers {
                if let audioOnlineVC = viewController as? AudioOnlineVC {
                    if audioOnlineVC.saveGenres.count > 0 {
                        fliterOfflineVC.savedGenres = audioOnlineVC.saveGenres
                    }
                }
            }
        }
        
        
        self.present(fliterOfflineVC, animated: true, completion: nil)
        
    }

    func loadData(number_book : Int, from : Int , book_type : BookType, succces : @escaping ([TopBook]) -> () ){
        var resultBooks : [TopBook] = []
            
        
            ManagerData.instance.getTopBook(number_book: number_book, from: from, book_type: book_type, success: { json, msg in
                
                for item in (json?.array)! {
                    resultBooks.append(TopBook(from: item))
                    self.collectionView.reloadData()
                    
                }
                self.fromBook += 10
                succces(resultBooks)
                
            }) { (msg) in
                print(msg)
            }
        

    }


    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if refreshControl.isRefreshing == true {
            self.books.removeAll()
            self.stories.removeAll()
            loadData(number_book: 10, from: 0, book_type: .text) { (result) in
                print(result)
                self.books = result
                self.stories = self.books
                self.showStories = self.stories
                self.collectionView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    @IBAction func actionSegmentControl(_ sender: Any) {
        
        if self.segmentControl.selectedSegmentIndex == 0 {
            self.collectionView.isHidden = false
        } else {
            self.collectionView.isHidden = true
        }
        
    }
    
    
}

extension OnlineVC : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return showStories.count + (showStories.count/6)
    }
    
    //--
         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.indexpathtest = indexPath.item
        if indexPath.row % 6 == 0 && indexPath.row != 0{
            let height = (collectionView.frame.width/3 - 10)*2/3
            let width = collectionView.frame.width - 13
            
            return CGSize(width: width , height: height)
            
        }else{
            let width = collectionView.frame.width/3 - 10
            let height = width*2 - 10
            print(width)
            return CGSize(width: width, height: height )
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
     
            return 9.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
     
            return 5.0
    }
    
    
    //-- select
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row % 6 == 0 && indexPath.row != 0{
            UIApplication.shared.openURL(URL(string: "https://www.google.com")!)
        }else{
            let indexPatchRow = indexPath.row - (indexPath.row/6)
            let detail = self.storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
            detail.data = showStories[indexPatchRow]
            
            detail.isAudioBook = false 
            
            navigationController?.pushViewController(detail, animated: true)
            
            self.endSearchBar()
        }}
    
    
    
}
extension OnlineVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row % 6 == 0 && indexPath.row != 0{
            print("1")
        }else{
            if showStories.count > 0 {
                let indexPatchRow = indexPath.row - (indexPath.row/6)
                if let stringURL = showStories[indexPatchRow].image {
                    let url = URL(string: "\(baseURL)\(stringURL)")
                    (cell as! FavoriteOnlineCell).imageBook.kf.setImage(with: url, options: [.transition(.fade(0.5))])
                }
                
                (cell as! FavoriteOnlineCell).books = showStories[indexPatchRow]
            }
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row % 6 == 0 && indexPath.row != 0{
            print("1")
        }else{
            (cell as! FavoriteOnlineCell).imageBook.kf.cancelDownloadTask()
        }}
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row % 6 == 0 && indexPath.row != 0 && indexPath.row >= 6 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell1", for: indexPath)  as! AdsCollectionViewCell
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellID, for: indexPath)  as! FavoriteOnlineCell
            cell.imageBook.kf.indicatorType = .activity
            return cell
        }}
}
// MARK: Search
extension OnlineVC {
    
    override func actionSearch(_ sender: UIBarButtonItem) {
        super.actionSearch(sender)
        
        
    }
    
    override func endSearchBar() {
        super.endSearchBar()
        
        //self.collectionViewTopConstraint.constant = 0
        self.view.layoutSubviews()
    }
    
    override func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        super.searchBarTextDidBeginEditing(searchBar)
    
    }
    
    override func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        super.searchBarCancelButtonClicked(searchBar)
        
        self.showStories = []
        self.showStories = self.stories
        self.collectionView.reloadData()
        
        if segmentControl.selectedSegmentIndex == 1 {
            for viewController in self.childViewControllers {
                if let audioOnlineVC = viewController as? AudioOnlineVC {
                    audioOnlineVC.showStories = []
                    audioOnlineVC.showStories = audioOnlineVC.stories
                    audioOnlineVC.audioCollectionView.reloadData()
                }
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchBar.text != "" {
            if self.segmentControl.selectedSegmentIndex == 0 {
                self.showStories = []
                self.collectionView.reloadData()
                for story in self.stories {
                    if (story.name?.uppercased().contains(searchBar.text!.uppercased()))! {
                        self.showStories.append(story)
                    }
                }
                self.collectionView.reloadData()
            } else {
            
                for viewController in self.childViewControllers {
                    if let audioOnlineVC = viewController as? AudioOnlineVC {
                        audioOnlineVC.showStories = []
                        for story in audioOnlineVC.stories {
                            if (story.name?.uppercased().contains(searchBar.text!.uppercased()))! {
                                audioOnlineVC.showStories.append(story)
                            }
                        }
                        audioOnlineVC.audioCollectionView.reloadData()
                        break
                    }
                }
            }
            
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.searchBar.isFirstResponder == true {
            endSearchBar()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("ok")
        endSearchBar()
    }
}

