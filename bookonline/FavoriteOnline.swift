//
//  FavoriteOnline.swift
//  bookonline
//
//  Created by tubjng on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift

class FavoriteOnline: UIViewController {
    
    @IBOutlet var collectionFavoriteOnline: UICollectionView!
    let baseURL = "http://bookonline.myaawu.com/"
    var realm: Realm?
    var favoriteBook = [FavoriteBook]()
    var stories = [FavoriteBook]()
    var topBookSend = TopBook()
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionFavoriteOnline.delegate = self
        collectionFavoriteOnline.dataSource = self
        self.collectionFavoriteOnline.register(UINib(nibName: "FavoriteOnlineCell", bundle: nil), forCellWithReuseIdentifier: "Cell")

        let layout = collectionFavoriteOnline.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.sectionHeadersPinToVisibleBounds = true
        NotificationCenter.default.addObserver(self, selector: #selector(sortName), name: NSNotification.Name(rawValue: "SortName"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortTime), name: NSNotification.Name(rawValue: "SortTime"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(searchBarFavoriteCancel), name: NSNotification.Name(rawValue: "SearchBarFavoriteCancel"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(searchFavoriteDidChane), name: NSNotification.Name(rawValue: "SearchFavoriteDidChange"), object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        realm = try? Realm()
        favoriteBook.removeAll()
        let objects = realm?.objects(FavoriteBook.self)
        for object in objects! {
            favoriteBook.append(object)
        }
        self.stories = self.favoriteBook
        collectionFavoriteOnline.reloadData()
    }
    func searchBarFavoriteCancel() {
                self.stories = []
                self.stories = self.favoriteBook
                self.collectionFavoriteOnline.reloadData()
    }
    func searchFavoriteDidChane(notification:NSNotification) {
        if let textInput = notification.userInfo?["text"] as? String{
            self.stories = []
                        for story in self.favoriteBook {
                            if story.name.uppercased().contains(textInput.uppercased()) {
                                self.stories.append(story)
                            }
                        }
                        self.collectionFavoriteOnline.reloadData()

        }
    }
    func sortName() {
        let tempFavoriteBook = stories.sorted { removeVietnamese(str: $0.name) < removeVietnamese(str: $1.name) }
        stories = tempFavoriteBook
        collectionFavoriteOnline.reloadData()
    }
    func sortTime() {
        let tempFavoriteBook = stories.sorted { $0.like_time < $1.like_time }
        stories = tempFavoriteBook
        collectionFavoriteOnline.reloadData()
    }
    
    func removeVietnamese(str: String) -> String {
        let vi = ["àảãáạăằẳẵắặâầẩẫấậÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ","èẻẽéẹêềểễếệÈẺẼÉẸÊỀỂỄẾỆ","ìỉĩíịÌỈĨÍỊ","òỏõóọôồổỗốộơờởỡớợÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ","ùủũúụưừửữứựÙỦŨÚỤƯỪỬỮỨỰ","ỳỷỹýỵỲỶỸÝỴ","đĐ"]
        let en = ["a", "e", "i", "o", "u", "y", "d"]
        var newStr = str
        for i in 0 ... vi.count - 1 {
            let old:String = vi[i]
            for j in 0 ... old.characters.count - 1 {
                newStr = newStr.replacingOccurrences(of: "\(old[j])", with: en[i])
            }
        }
        newStr = newStr.lowercased()
        
        return newStr
    }

    func convert(book: FavoriteBook) -> TopBook {
        let topBook = TopBook()
        
        topBook.name = book.name
        topBook.author = book.author
        topBook.book_source = book.book_source
        topBook.book_type = book.book_type
        topBook.category_id = book.category_id
        topBook.category_name = book.category_name
        topBook.chap_number = book.chap_number
        topBook.country_id = book.country_id
        topBook.create_date = book.create_date
        topBook.description = book.descriptionBook
        topBook.id = book.id
        topBook.image = book.image
        topBook.isbn = book.isbn
        topBook.language_type = book.language_type
        topBook.last_update = book.last_update
        topBook.num_view = book.num_view
        topBook.page_number = book.page_number
        
        return topBook
    }
}
extension FavoriteOnline:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cell = collectionFavoriteOnline.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as? FavoriteHeaderCell
        
        cell?.numberBooks.text = "\(stories.count)"
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Online", bundle: nil)
        let detail = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        let topBook = convert(book: stories[indexPath.item])

        detail.data = topBook
        if topBook.book_type == Constant.BookTypeAudio {
            detail.isAudioBook = true
        } else {
            detail.isAudioBook = false
        }
        
        navigationController?.pushViewController(detail, animated: true)
        if let FavoriteVC = self.parent as? FavoriteVC {
            FavoriteVC.endSearchBar()
        }

    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionFavoriteOnline.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! FavoriteOnlineCell
        cell.authorBook.text = stories[indexPath.row].author
        cell.titleBook.text = stories[indexPath.row].name
        
           let stringURL = stories[indexPath.item].image
         if stringURL != ""{
            let url = URL(string: "\(baseURL)\(stringURL)")
            cell.imageBook.kf.setImage(with: url)
        }


        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width/3 - 10
        return CGSize(width: width, height: 2 * width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3.0
    }



}
