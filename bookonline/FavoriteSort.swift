//
//  FavoriteSort.swift
//  bookonline
//
//  Created by tubjng on 1/20/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift
class FavoriteSort: UITableViewController {

    @IBOutlet var nameImageView: UIImageView!
    @IBOutlet var popViewTableView: UITableView!
    @IBOutlet var timeImageView: UIImageView!
    
    var indexSave = [FavoriteBookSort]()
    var realm: Realm!
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()
        self.view.layer.cornerRadius = 10.0
        self.view.layer.borderWidth = 1.5
        self.view.layer.borderColor = UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0).cgColor
        self.navigationController?.isNavigationBarHidden = true
        self.popViewTableView.alwaysBounceVertical = false

    }
    override func viewWillAppear(_ animated: Bool) {
        realm = try? Realm()
         let objects = realm.objects(FavoriteBookSort.self)
        if objects.count == 0{
            let favoriteBookSort = FavoriteBookSort()
            try! realm.write {
                favoriteBookSort.selectionIndexAt = 0
                realm.add(favoriteBookSort)
            }}
            for object in objects{
            indexSave.append(object)
            selectedIndex = object.selectionIndexAt
        }
        
        if self.selectedIndex == 0 {
            nameImageView.image = UIImage(named: "CheckPoint")
            timeImageView.image = UIImage(named: "Circle")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SortName"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SortNameOffline"), object: nil)
        } else {
            nameImageView.image = UIImage(named: "Circle")
            timeImageView.image = UIImage(named: "CheckPoint")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SortTime"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SortTimeOffline"), object: nil)
        }

    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.selectedIndex = indexPath.row

            if self.selectedIndex == 0 {
                
                realm = try? Realm()
                let objects = realm.objects(FavoriteBookSort.self)
                for object in objects{
                    try! realm.write {
                        object.selectionIndexAt = 0
                    }
                    
                }
                nameImageView.image = UIImage(named: "CheckPoint")
                timeImageView.image = UIImage(named: "Circle")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SortName"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SortNameOffline"), object: nil)
            } else {
                
                realm = try? Realm()
                let objects = realm.objects(FavoriteBookSort.self)
                for object in objects{
                    try! realm.write {
                        object.selectionIndexAt = 1
                        
                    }

                }
                nameImageView.image = UIImage(named: "Circle")
                timeImageView.image = UIImage(named: "CheckPoint")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SortTime"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SortTimeOffline"), object: nil)
            }
        }
    }

}
