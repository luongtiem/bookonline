//
//  RecentlyOnline.swift
//  bookonline
//
//  Created by tubjng on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift
class RecentlyOnline: UIViewController {
    @IBOutlet var tableRecentlyOnline: UITableView!
    var realm: Realm?
    var recentlyBook = [RecentlyBook]()
    var stories = [RecentlyBook]()
    let baseURL = "http://bookonline.myaawu.com/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try? Realm()
        tableRecentlyOnline.delegate = self
        tableRecentlyOnline.dataSource = self
        tableRecentlyOnline.tableFooterView = UIView()
        self.tableRecentlyOnline.register(UINib(nibName: "RecentlyOnlineCell", bundle: nil), forCellReuseIdentifier: "Cell")
        self.tableRecentlyOnline.register(UINib(nibName: "FavoriteSectionTitle", bundle: Bundle.main), forCellReuseIdentifier: "header")
        NotificationCenter.default.addObserver(self, selector: #selector(searchBarRecentlyCancel), name: NSNotification.Name(rawValue: "searchBarRecentlyCancel"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(searchRecentlyDidChange), name: NSNotification.Name(rawValue: "searchRecentlyDidChange"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        realm = try? Realm()
        recentlyBook.removeAll()
        let objects = realm?.objects(RecentlyBook.self)
        for object in objects! {
            recentlyBook.append(object)
        }
        self.stories = self.recentlyBook
        tableRecentlyOnline.reloadData()
    }
    func searchBarRecentlyCancel() {
        self.stories = []
        self.stories = self.recentlyBook
        self.tableRecentlyOnline.reloadData()
    }
    func searchRecentlyDidChange(notification:NSNotification) {
        if let textInput = notification.userInfo?["text"] as? String{
            self.stories = []
            for story in self.recentlyBook {
                if story.name.uppercased().contains(textInput.uppercased()) {
                    self.stories.append(story)
                }
            }
            self.tableRecentlyOnline.reloadData()
            
        }
    }
    
    func convert(book: RecentlyBook) -> TopBook {
        let topBook = TopBook()
        topBook.name = book.name
        topBook.author = book.author
        topBook.book_source = book.book_source
        topBook.book_type = book.book_type
        topBook.category_id = book.category_id
        topBook.category_name = book.category_name
        topBook.chap_number = book.chap_number
        topBook.country_id = book.country_id
        topBook.create_date = book.create_date
        topBook.description = book.descriptionBook
        topBook.id = book.id
        topBook.image = book.image
        topBook.isbn = book.isbn
        topBook.language_type = book.language_type
        topBook.last_update = book.last_update
        topBook.num_view = book.num_view
        topBook.page_number = book.page_number
        topBook.last_time = book.last_time
        return topBook
    }
}


extension RecentlyOnline: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Online", bundle: nil)
        let book = stories[indexPath.row]
        
        let bookRecently = convert(book: book)
        let detail = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        detail.data = bookRecently
        
        if detail.data.book_type == Constant.BookTypeAudio {
            detail.isAudioBook = true
        } else {
            detail.isAudioBook = false 
        }
        
        navigationController?.pushViewController(detail, animated: true)
        if let recentlyVC = self.parent as? RecentlyVC {
            recentlyVC.endSearchBar()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stories.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as? FavoriteSectionTitle
        cell?.numbBook.text = String(stories.count)
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableRecentlyOnline.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecentlyOnlineCell
        cell.author.text = stories[indexPath.row].author
        cell.nameTitle.text = stories[indexPath.row].name
        cell.selectionStyle = .none
        cell.display(recentlyBook: stories[indexPath.row])
        let stringURL = stories[indexPath.item].image
        if stringURL != ""{
            let url = URL(string: "\(baseURL)\(stringURL)")
            cell.imageBook.kf.setImage(with: url)
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    
}
