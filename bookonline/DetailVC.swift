//
//  DetailVC.swift
//  bookonline
//
//  Created by ReasonAmu on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift
import SlideMenuControllerSwift


let k_Size = "DownloadSize"

class DetailVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleBook: UILabel!
    @IBOutlet weak var authorBook: UILabel!
    @IBOutlet weak var totalChapter: UILabel!
    @IBOutlet weak var status: UILabel!
    
    var btnDownload = UIBarButtonItem()
    var btnFavorite = UIBarButtonItem()
    var favoriteBook = [FavoriteBook]()
    var realm: Realm!
    let cellTable = "CellTable"
    let baseURL = "http://bookonline.myaawu.com"
    let colorBackground = UIColor(red: 67/255, green:78/255, blue:93/255, alpha:1.0)
    
    var data: TopBook!
    var data2 : ModelDetailBook!
    
    var checkRecently = 0
    var liked = false
    var checkView = true
    var isOffline: Bool = false
    var bookChapters : [BookChapters] = []
    var imageStory: UIImage!
    
    var isAudioBook: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        realm = try! Realm()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.black
        tableView.register( UINib(nibName: "CellDetail", bundle: nil), forCellReuseIdentifier: cellTable)
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: (UIScreen.main.bounds.height/4))
        headerView.backgroundColor = colorBackground
        tableView.tableHeaderView = headerView
        view.backgroundColor = colorBackground
        
        btnFavorite = UIBarButtonItem(image: UIImage(named: "Favorite"), style: .plain, target: self, action: #selector(handleFavorite))
        btnDownload = UIBarButtonItem(image: #imageLiteral(resourceName: "Download").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleDownload))
        setupUI()
        navigationItem.rightBarButtonItems = [btnDownload,btnFavorite]


        requestData()

//        if isAudioBook != nil {
//            if isAudioBook == true {
//                let indexPath = IndexPath(row: 0, section: 0)
//                let cell = self.tableView.cellForRow(at: indexPath) as! CellDetail
//                cell.btnListenBook.isHidden = false
//            }
//        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if checkView{
            realm = try? Realm()
            let objects = realm.objects(FavoriteBook.self)
            liked = false
            for object in objects{
                if data.id == object.id
                {
                    liked = true
                    break
                }
            }
            if liked {
                btnFavorite.image = UIImage(named: "FavoriteChecked")
            }
            else{
                btnFavorite.image = UIImage(named: "Favorite")
            }
        }else{
            realm = try? Realm()
            let objects = realm.objects(FavoriteBookOffline.self)
            liked = false
            for object in objects{
                if data.id == object.id
                {
                    liked = true
                    break
                }
            }
            if liked {
                btnFavorite.image = UIImage(named: "FavoriteChecked")
            }
            else{
                btnFavorite.image = UIImage(named: "Favorite")
            }}
    }
    //-- UI
    func setupUI(){
        
        if isOffline == false {
            if let stringURL = data.image {
                let url = URL(string: "\(baseURL)\(stringURL)")
                imageView.kf.setImage(with: url, options: [.transition(.fade(0.5))])
            }
        } else {
            if self.imageStory != nil {
                imageView.image = imageStory
            }
        }
        
        
        titleBook.text = data.name
        authorBook.text  = data.author
        if let chapter = data.chap_number {
            totalChapter.text = chapter + R.string.bookonline.chap()
        }
        
        status.text = R.string.bookonline.translating()
        
        
        
        //        btnFavorite = UIBarButtonItem(image: #imageLiteral(resourceName: "Favorite").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(handleFavorite))
        //        btnDownload = UIBarButtonItem(image: #imageLiteral(resourceName: "Download").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleDownload))
        
        
        
        data2 = ModelDetailBook(sourceBook: data.book_source!, category: data.category_name!, contentBook:  data.description)
        
        let objects = realm.objects(OfflineStory.self)
        
        for object in objects {
            if object.id == self.data.id {
                btnDownload.isEnabled = false
                
                break
            }
        }
        
        
    }
    
    func requestData() {
        
        if isOffline == false {
            self.bookChapters = []
            ManagerData.instance.getDetailBook(book_id: self.data.id!, success: { json, msg in
                
                for item in (json?.array)! {
                    self.bookChapters.append(BookChapters(from: item))
                }
                
                if self.bookChapters.count > 0 && self.bookChapters[0].file_audio != nil {
                    let indexPath = IndexPath(row: 0, section: 0)
                    let cell = self.tableView.cellForRow(at: indexPath) as! CellDetail
                    cell.btnListenBook.isHidden = false
                    
                    if self.isAudioBook != nil {
                        if self.isAudioBook == true  {
                            cell.btnListenBook.isHidden = false
                            cell.btnReadBook.isHidden = true
                        } else {
                            cell.btnListenBook.isHidden = true
                            cell.btnReadBook.isHidden = false
                        }
                    }
                }
                
                
                
            }) { (msg) in
                print(msg)
            }
        }
        else {
            
            if self.bookChapters[0].file_audio != nil && self.bookChapters[0].file_audio != "" {
                let indexPath = IndexPath(row: 0, section: 0)
                let cell = self.tableView.cellForRow(at: indexPath) as! CellDetail
                cell.btnListenBook.isHidden = false
                
                if self.isAudioBook != nil {
                    if self.isAudioBook == true  {
                        cell.btnListenBook.isHidden = false
                        cell.btnReadBook.isHidden = true
                    } else {
                        cell.btnListenBook.isHidden = true
                        cell.btnReadBook.isHidden = false
                    }
                }
            }
            
        }
        
    }
    
    func handleFavorite(){
        if checkView{
            if !liked {
                try! realm.write {
                    
                    let favoriteBook = FavoriteBook()
                    if let author = data.author {
                        favoriteBook.author = author
                    }
                    if let id = data.id{
                        favoriteBook.id = id
                    }
                    if let image = data.image{
                        favoriteBook.image = image
                    }
                    if let name = data.name{
                        favoriteBook.name = name
                    }
                    if let descriptionBook = data.description{
                        favoriteBook.descriptionBook = descriptionBook
                    }
                    if let book_source = data.book_source{
                        favoriteBook.book_source = book_source
                    }
                    if let country_id = data.country_id{
                        favoriteBook.country_id = country_id
                    }
                    if let category_id = data.category_id{
                        favoriteBook.category_id = category_id
                    }
                    if let page_number = data.page_number{
                        favoriteBook.page_number = page_number
                    }
                    if let publisher = data.publisher{
                        favoriteBook.publisher = publisher
                    }
                    if let publish_date = data.publish_date{
                        favoriteBook.publish_date = publish_date
                    }
                    if let create_date = data.create_date{
                        favoriteBook.create_date = create_date
                    }
                    if let last_update = data.last_update{
                        favoriteBook.last_update = last_update
                    }
                    if let isbn = data.isbn{
                        favoriteBook.isbn = isbn
                    }
                    if let language_type = data.language_type{
                        favoriteBook.language_type = language_type
                    }
                    
                    if let book_type = data.book_type{
                        favoriteBook.book_type = book_type
                    }
                    
                    if let chap_number = data.chap_number{
                        favoriteBook.chap_number = chap_number
                    }
                    if let num_view = data.num_view{
                        favoriteBook.num_view = num_view
                    }
                    if let category_name = data.category_name{
                        favoriteBook.category_name = category_name
                    }
                    favoriteBook.like_time = Int(Date().timeIntervalSince1970)
                    let objects = realm.objects(FavoriteBook.self)
                    realm.add(favoriteBook)
                    btnFavorite.image = UIImage(named: "FavoriteChecked")
                    liked = true
                }
                
            } else {
                
                try! realm.write {
                    let objects = realm.objects(FavoriteBook.self)
                    for obj in objects{
                        if obj.id == data.id{
                            realm.delete(obj)
                            break
                        }
                    }
                    
                    btnFavorite.image = UIImage(named: "Favorite")
                    liked = false
                    //
                }
                
            }}
        else{
            if !liked {
                try! realm.write {
                    
                    let favoriteBookOffline = FavoriteBookOffline()
                    if let author = data.author {
                        favoriteBookOffline.author = author
                    }
                    if let id = data.id{
                        favoriteBookOffline.id = id
                    }
                    if let image = data.image{
                        favoriteBookOffline.image = image
                    }
                    if let name = data.name{
                        favoriteBookOffline.name = name
                    }
                    if let descriptionBook = data.description{
                        favoriteBookOffline.bookDescription = descriptionBook
                    }
                    if let book_source = data.book_source{
                        favoriteBookOffline.book_source = book_source
                    }
                    if let country_id = data.country_id{
                        favoriteBookOffline.country_id = country_id
                    }
                    if let category_id = data.category_id{
                        favoriteBookOffline.category_id = category_id
                    }
                    if let page_number = data.page_number{
                        favoriteBookOffline.page_number = page_number
                    }
                    if let publisher = data.publisher{
                        favoriteBookOffline.publisher = publisher
                    }
                    if let publish_date = data.publish_date{
                        favoriteBookOffline.publish_date = publish_date
                    }
                    if let create_date = data.create_date{
                        favoriteBookOffline.create_date = create_date
                    }
                    if let last_update = data.last_update{
                        favoriteBookOffline.last_update = last_update
                    }
                    if let isbn = data.isbn{
                        favoriteBookOffline.isbn = isbn
                    }
                    if let language_type = data.language_type{
                        favoriteBookOffline.language_type = language_type
                    }
                    
                    if let book_type = data.book_type{
                        favoriteBookOffline.book_type = book_type
                    }
                    
                    if let chap_number = data.chap_number{
                        favoriteBookOffline.chap_number = chap_number
                    }
                    if let num_view = data.num_view{
                        favoriteBookOffline.num_view = num_view
                    }
                    if let category_name = data.category_name{
                        favoriteBookOffline.category_name = category_name
                    }
                    favoriteBookOffline.like_time = Int(Date().timeIntervalSince1970)
                    print(favoriteBookOffline.like_time)
                    let objects = realm.objects(FavoriteBookOffline.self)
                    realm.add(favoriteBookOffline)
                    btnFavorite.image = UIImage(named: "FavoriteChecked")
                    liked = true
                }
                
            } else {
                
                try! realm.write {
                    let objects = realm.objects(FavoriteBookOffline.self)
                    for obj in objects{
                        if obj.id == data.id{
                            realm.delete(obj)
                            break
                        }
                    }
                    
                    btnFavorite.image = UIImage(named: "Favorite")
                    liked = false
                    //
                }
                
            }
            
        }
    }
    
    func handleDownload(){
        
        
        self.btnDownload.isEnabled = false
        
        let offlineStory = OfflineStory()
        
        offlineStory.id = self.data.id != nil ? self.data.id! : ""
        offlineStory.name = self.data.name != nil ? self.data.name! : ""
        offlineStory.image = self.data.image != nil ? self.data.image! : ""
        offlineStory.bookDescription = self.data.description != nil ? self.data.description! : ""
        offlineStory.book_source = self.data.book_source != nil ? self.data.book_source! : ""
        offlineStory.author = self.data.author != nil ? self.data.author! : ""
        offlineStory.country_id = self.data.country_id != nil ? self.data.country_id! : ""
        offlineStory.category_id = self.data.category_id != nil ? self.data.category_id! : ""
        offlineStory.page_number = self.data.page_number != nil ? self.data.page_number! : ""
        offlineStory.publisher = self.data.publisher != nil ? self.data.publisher! : ""
        offlineStory.publish_date = self.data.publish_date != nil ? self.data.publish_date! : ""
        offlineStory.create_date = self.data.create_date != nil ? self.data.create_date! : ""
        offlineStory.last_update = self.data.last_update != nil ? self.data.last_update! : ""
        offlineStory.isbn = self.data.isbn != nil ? self.data.isbn! : ""
        offlineStory.language_type = self.data.language_type != nil ? self.data.language_type! : ""
        offlineStory.book_type = self.data.book_type != nil ? self.data.book_type! : ""
        offlineStory.chap_number = self.data.chap_number != nil ? self.data.chap_number! : ""
        offlineStory.num_view = self.data.num_view != nil ? self.data.num_view! : ""
        offlineStory.category_name = self.data.category_name != nil ? self.data.category_name! : ""
        
        if UserDefaults.standard.value(forKey: k_Size) != nil {
            let size = UserDefaults.standard.value(forKey: k_Size) as! Int
            let newSize = size + malloc_size(offlineStory.accessibilityElements)
            
            UserDefaults.standard.set(newSize, forKey: k_Size)
        } else {
            let newSize = malloc_size(offlineStory.accessibilityElements)
            UserDefaults.standard.set(newSize, forKey: k_Size)
        }
        
        if self.data.image != nil {
            let imageUrl = URL(string: baseUrl + self.data.image!)
            KingfisherManager.shared.retrieveImage(with: imageUrl!, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, Url) in
                
                if image != nil  {
                    offlineStory.imageData = UIImagePNGRepresentation(image!)! as NSData?
                
                        let size = UserDefaults.standard.value(forKey: k_Size) as! Int
                        let newSize = size + (offlineStory.imageData?.length)!
                        UserDefaults.standard.set(newSize, forKey: k_Size)
    
                }
                
            })
        }
        
        ManagerData.instance.getDetailBook(book_id: self.data.id!, success: { (json, msg) in

            DispatchQueue.global(qos: .default).async {
                
                for item in (json?.array)! {
                    let bookChapter = BookChapters(from: item)
                    let offlineChapter = OfflineChapter()
                    offlineChapter.content = bookChapter.content != nil ? bookChapter.content! : ""
                    offlineChapter.chap_name = bookChapter.chap_name != nil ? bookChapter.chap_name! : ""
                    offlineChapter.chap_no = bookChapter.chap_no != nil ? bookChapter.chap_no! : ""
                    offlineChapter.file_audio = bookChapter.file_audio != nil ? bookChapter.file_audio! : ""
                    
                    offlineStory.chapters.append(offlineChapter)
                    
                
                    let size = UserDefaults.standard.value(forKey: k_Size) as! Int
                    
                    let newSize = size + offlineChapter.content.utf8.count
                    
                    UserDefaults.standard.set(newSize, forKey: k_Size)

                    if offlineChapter.file_audio != "" {
                        
                        self.downloadSong(song: bookChapter)
                        
                        offlineChapter.file_audio = "/\(bookChapter.name!)/\(bookChapter.chap_name!).mp3"

                    }
                }
                
                let realm1 = try! Realm()
                
                try! realm1.write {
                    realm1.add(offlineStory)
                    self.btnDownload.isEnabled = false
                }
                
                
            }
            
        }) { (msg) in
            print(msg)
        }

        
    }
    func downloadSong(song: BookChapters) {
        
            let url = URL(string: baseUrl + song.file_audio!)
            print(url)
            let dataSong = try? Data(contentsOf: url!)
        
        
        
            if let dir = kDOCUMENT_DIRECTORY_PATH {
                let pathToWriteSong = "\(dir)/\(song.name!)"
                print(pathToWriteSong)
                do {
                    
                    try FileManager.default.createDirectory(atPath: pathToWriteSong, withIntermediateDirectories: false, attributes: nil)
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                
                self.writeDataToPath(dataSong!, path: "\(pathToWriteSong)/\(song.chap_name!).mp3")
                
                let path = "\(pathToWriteSong)/\(song.chap_name!).mp3"
                let attributes = try! FileManager.default.attributesOfItem(atPath: path)
                let fileSize = attributes[FileAttributeKey.size] as! Int

                let size = UserDefaults.standard.value(forKey: k_Size) as! Int
                let newSize = size + fileSize
                UserDefaults.standard.set(newSize, forKey: k_Size)
            }
        
    }
    

    
    

    func writeDataToPath(_ data: Data, path: String) {
        do {
            try data.write(to: URL(fileURLWithPath: path), options: [.atomic])
        } catch {
            print(error.localizedDescription)
        }
        
        
    }

    
}

extension DetailVC : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension DetailVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellTable, for: indexPath) as! CellDetail
        cell.detailVC = self
        cell.data = data
        cell.setupDetail = data2
        cell.checkRead()
        if checkRecently == 1{
            cell.checkRecently = false
        }
        return cell
    }
    
}

extension DetailVC {
    
    func presentReadBook() {
        let readBook =  self.storyboard?.instantiateViewController(withIdentifier: "ReadBookVC") as! ReadBookVC

        readBook.idBook = data.id
        if isOffline {
            readBook.isOffline = true
            readBook.bookChapters = self.bookChapters
        }

        
        readBook.idBook = data.id
        readBook.bookChapters = self.bookChapters
        
        if isOffline {
            readBook.isOffline = true
        } 

        let leftMenu = self.storyboard?.instantiateViewController(withIdentifier: "LeftMenuReadBook") as! LeftMenuReadBook
        let slideMenu = SlideMenuController(mainViewController: readBook, leftMenuViewController: leftMenu)
        SlideMenuOptions.hideStatusBar = false
        
        self.present(slideMenu, animated: true, completion: nil)
        
    }
    
    func presentListenBook() {
        let listenBookVC = ListenBookVC(nibName: "ListenBookVC", bundle: nil)
        
        listenBookVC.isOffline = self.isOffline
        listenBookVC.topBook = self.data
        listenBookVC.listChapters = []
        for chapter in self.bookChapters {
            if chapter.file_audio != nil && chapter.file_audio != "" {
                listenBookVC.listChapters.append(chapter)
            }
        }

        if self.imageStory != nil {
            listenBookVC.imageStory = self.imageStory
        }
        
        self.present(listenBookVC, animated: true, completion: nil)
    }
}
