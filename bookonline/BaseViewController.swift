//
//  BaseViewController.swift
//  bookonline
//
//  Created by Enrik on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class BaseViewController: BaseVC {

    let kOriginViewSearchFrame = CGRect(x: UIScreen.main.bounds.width, y: 0, width: 0, height: 64)
    let kAfterViewSearchFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 64)
    var kOriginSelfView: CGRect!
    
    var searchButton: UIBarButtonItem!
    var searchBar: UISearchBar!
    var tapOutSearchBar: UIGestureRecognizer!
    var blurView: UIView!
    var viewSearchBar: UIView!
    var button2: UIBarButtonItem!
    
    let placeHolderText = R.string.bookonline.search()
 

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        //setupBlurView()
        setupSearchBar()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        navigationController?.navigationBar.isTranslucent = false
        
        addSearchButton()
        
    }
    
    
    func addSearchButton (){
        searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(actionSearch(_:)))
        self.navigationItem.rightBarButtonItems = [searchButton]
    }
    
    func addButton2(imageName: String) {
        button2 = UIBarButtonItem(image: UIImage(named: imageName), style: .plain, target: self, action: #selector(actionBtn2))
        self.navigationItem.rightBarButtonItems?.append(button2)
    }
    
    func actionBtn2() {
        print("ok")
    }
    
    func setupBlurView() {
        blurView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        blurView.backgroundColor = UIColor.clear
        self.view.addSubview(blurView)
        blurView.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        kOriginSelfView = self.view.frame

    }
    
    func setupSearchBar() {
        viewSearchBar = UIView(frame: kOriginViewSearchFrame)
        viewSearchBar.alpha = 0
        viewSearchBar.isHidden = true
        viewSearchBar.backgroundColor = UIColor.black
        
        searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.barTintColor = UIColor.black
        viewSearchBar.addSubview(searchBar)
        
        let paddingLeft = Autolayout.shareInstance.paddingLeftInside(viewCon: searchBar, viewCha: viewSearchBar, pixel: 0)
        let paddingRight = Autolayout.shareInstance.paddingRightInside(viewCon: searchBar, viewCha: viewSearchBar, pixel: 0)
        let paddingBottom = Autolayout.shareInstance.paddingBottomInside(viewCon: searchBar, viewCha: viewSearchBar, pixel: 0)
    
        viewSearchBar.translatesAutoresizingMaskIntoConstraints = true
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        viewSearchBar.addConstraints([paddingLeft,paddingRight,paddingBottom])
        
        self.view.addSubview(viewSearchBar)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        
    }
    
    func actionSearch(_ sender: UIBarButtonItem) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.viewSearchBar.isHidden = false
            self.viewSearchBar.alpha = 1
            self.searchBar.showsCancelButton = true
            self.searchBar.becomeFirstResponder()
            self.viewSearchBar.frame = self.kAfterViewSearchFrame
            self.viewSearchBar.layoutIfNeeded()

            //self.blurView.isHidden = false
            
            self.view.layoutSubviews()
        }) { (completed) in
            
        }
        
    }
    
}

extension BaseViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        endSearchBar()
        
    }
    
    func endSearchBar(){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.viewSearchBar.frame = self.kOriginViewSearchFrame
            
            self.viewSearchBar.alpha = 0
            
            self.searchBar.resignFirstResponder()
            
            self.view.layoutSubviews()
            
        }) { (completed) in
            
            self.viewSearchBar.isHidden = true
            //self.blurView.isHidden = true
        }
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        tapOutSearchBar = UITapGestureRecognizer(target: self, action: #selector(endSearchBar))
        //self.blurView.addGestureRecognizer(tapOutSearchBar)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //self.blurView.removeGestureRecognizer(tapOutSearchBar)
    }
}

