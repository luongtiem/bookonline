//
//  LeftMenuReadBook.swift
//  bookonline
//
//  Created by ReasonAmu on 1/14/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
protocol SendDataChapterBook {
    
    func sendDataBook(chapter: BookChapters)
}

class LeftMenuReadBook: UIViewController {
    
    let cellID = "CellSildeMenu"
    var totalChapter : [BookChapters] = []
    var readBook :ReadBookVC?
    
    
    
    //MARK: SETTINGS MENU
    lazy var settingLauncher : SetupContent  = {
        let setting =  SetupContent()
        setting.leftMenu = self
        setting.readBook = self.readBook
        return setting
    }()
    
    
    @IBOutlet weak var btnSetupContent: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCancel.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        btnSetupContent.addTarget(self, action: #selector(handleSetupContent), for: .touchUpInside)
        
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
}

extension LeftMenuReadBook {
    
    
    func handleCancel(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleSetupContent(){
        settingLauncher.showSetupContent()
        self.slideMenuController()?.toggleLeft()
        
    }
    
}

extension LeftMenuReadBook: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        return totalChapter.count
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        readBook?.sendDataBook(chapter: totalChapter[indexPath.row])
        readBook?.viewTitleShow.removeFromSuperview()
        self.slideMenuController()?.closeLeft()
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            return 20
        }else{
            return 0.00001
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    
}

extension LeftMenuReadBook : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        
        if let chap_no = totalChapter[indexPath.row].chap_no, let chap_name = totalChapter[indexPath.row].chap_name{
            cell.textLabel?.text = chap_no + " : " + chap_name
        }
        return cell
        
    }
    
}
