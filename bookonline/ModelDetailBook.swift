//
//  ModelDetailBook.swift
//  bookonline
//
//  Created by ReasonAmu on 1/13/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import Foundation

class ModelDetailBook {
    
    var sourceBook :String?
    var category: String?
    var contentBook : String?

    
    init(sourceBook : String , category : String ,contentBook : String?) {
        self.sourceBook = sourceBook
        self.category = category
        self.contentBook = contentBook
    }
    
}
