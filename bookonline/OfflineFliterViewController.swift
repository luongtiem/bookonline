//
//  OfflineFliterViewController.swift
//  bookonline
//
//  Created by Enrik on 1/11/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class OfflineFliterViewController: UIViewController {
    
    let headerIdentifier = "HeaderCollectionCell"
    let cellStateIdentifier = "StateCollectionCell"
    let cellGenreIdentifier = "GenreCollectionCell"
    
    var offlineVC: OfflineVC!
    var onlineVC: OnlineVC!
    
    var isOffline: Bool = true
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //var genre = [String]()
    var headerTitles = [R.string.bookonline.status(),R.string.bookonline.category()]
    var states = ["Tất cả","Mới cập nhật","Đã tải"]
    var genres = [R.string.bookonline.aZ(), "Kinh di", "Than thoai", "Hai huoc", "Hanh dong", "Ngon tinh", "Khong xac dinh", "Che"]
    
    var saveGenres = [R.string.bookonline.aZ()]
    var savedGenres: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        
    }
    
    func setupCollectionView() {
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let nibHeader = UINib(nibName: "FliterHeaderReusableView", bundle: nil)
        collectionView.register(nibHeader, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerIdentifier)
        
        let nibState = UINib(nibName: "FliterStateCell", bundle: nil)
        collectionView.register(nibState, forCellWithReuseIdentifier: cellStateIdentifier)
        
        let nibGenre = UINib(nibName: "OfflineFliterGenreCell", bundle: nil)
        collectionView.register(nibGenre, forCellWithReuseIdentifier: cellGenreIdentifier)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionSave(_ sender: Any) {
        saveGenres = []
        for index in 0..<genres.count {
            let indexPath = IndexPath(item: index, section: 0)
            let cell = collectionView.cellForItem(at: indexPath) as! OfflineFliterGenreCell
            if cell.state == .add {
                saveGenres.append(cell.title)
            }
        }
        if isOffline {
            
            if offlineVC.segmentControl.selectedSegmentIndex == 0 {
                offlineVC.saveGenres = self.saveGenres
            } else {
                let audioOfflineVC = offlineVC.childViewControllers[0] as! AudioOfflineVC
                audioOfflineVC.saveGenres = self.saveGenres
            }

        } else {
            if onlineVC.segmentControl.selectedSegmentIndex == 0 {
                onlineVC.saveGenres = self.saveGenres
            } else {
                let audioOnlineVC = onlineVC.childViewControllers[0] as! AudioOnlineVC
                audioOnlineVC.saveGenres = self.saveGenres
            }
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension OfflineFliterViewController: UICollectionViewDelegate, UICollectionViewDataSource  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return genres.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellGenreIdentifier, for: indexPath) as! OfflineFliterGenreCell
        
        let genre = genres[indexPath.item]
        cell.btnGenre.setTitle(genre, for: .normal)
        cell.title = genres[indexPath.item]
        
        if savedGenres.count > 0 {
            var isInSaved = false
            for savedGenre in savedGenres {
                if genre == savedGenre {
                    isInSaved = true
                    break
                }
            }
            if isInSaved == true {
                cell.changeToStateAdd()
            }
            
        }

        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {


            let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerIdentifier, for: indexPath) as! FliterHeaderReusableView
            
            headerCell.imageViewHeader.image = UIImage(named: "Category")
            headerCell.labelHeaderTitle.text = self.headerTitles[1]

            return headerCell

        
    }
    
}

extension OfflineFliterViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let label = UILabel()
        label.text = genres[indexPath.item]
        label.sizeToFit()
        let frame = label.frame
        return CGSize(width: frame.width + 15, height: 25)

    }
}

// Phần code chứa trạng thái + thể loại
/*
 extension OfflineFliterViewController: UICollectionViewDelegate, UICollectionViewDataSource  {
 
 func numberOfSections(in collectionView: UICollectionView) -> Int {
 return 2
 }
 
 func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 if section == 0 {
 return states.count
 } else {
 return genres.count
 }
 }
 
 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 
 switch indexPath.section {
 case 0:
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellStateIdentifier, for: indexPath) as! FliterStateCell
 
 cell.btnState.setTitle(states[indexPath.item], for: .normal)
 
 return cell
 default:
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellGenreIdentifier, for: indexPath) as! OfflineFliterGenreCell
 
 cell.btnGenre.setTitle(genres[indexPath.item], for: .normal)
 
 return cell
 }
 }
 
 func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
 switch kind {
 case UICollectionElementKindSectionHeader:
 let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerIdentifier, for: indexPath) as! FliterHeaderReusableView
 
 switch indexPath.section {
 case 0:
 headerCell.imageViewHeader.image = UIImage(named: "Star_filled")
 headerCell.labelHeaderTitle.text = self.headerTitles[0]
 default:
 headerCell.imageViewHeader.image = UIImage(named: "Star_not_filled")
 headerCell.labelHeaderTitle.text = self.headerTitles[1]
 }
 
 return headerCell
 default:
 assert(false, "Unexpected element kind")
 }
 
 }
 
 }
 
 extension OfflineFliterViewController: UICollectionViewDelegateFlowLayout {
 
 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
 
 return CGSize(width: UIScreen.main.bounds.width, height: 40)
 }
 
 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
 
 switch indexPath.section {
 case 0:
 let label = UILabel()
 label.text = states[indexPath.item]
 label.sizeToFit()
 let frame = label.frame
 return CGSize(width: frame.width + 10, height: 25)
 default:
 let label = UILabel()
 label.text = genres[indexPath.item]
 label.sizeToFit()
 let frame = label.frame
 return CGSize(width: frame.width + 10, height: 25)
 }
 
 }
 
 //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
 //        return 5.0
 //    }
 //
 //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
 //        return 5.0
 //    }
 
 }
 */
