//
//  FavoriteOffline.swift
//  bookonline
//
//  Created by tubjng on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift
class FavoriteOffline: UIViewController {
    @IBOutlet var tableFavoriteOffline: UITableView!
    var favoriteBookOffline = [FavoriteBookOffline]()
    var stories = [FavoriteBookOffline]()
    var realm: Realm?
    let baseURL = "http://bookonline.myaawu.com/"
    let topBookSend = TopBook()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableFavoriteOffline.delegate = self
        tableFavoriteOffline.dataSource = self
        self.tableFavoriteOffline.register(UINib(nibName: "FavoriteOfflineCell", bundle: nil), forCellReuseIdentifier: "Cell")
        self.tableFavoriteOffline.register(UINib(nibName: "FavoriteSectionTitle", bundle: Bundle.main), forCellReuseIdentifier: "header")
        NotificationCenter.default.addObserver(self, selector: #selector(sortName), name: NSNotification.Name(rawValue: "SortNameOffline"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortTime), name: NSNotification.Name(rawValue: "SortTimeOffline"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(searchBarFavoriteOfflineCancel), name: NSNotification.Name(rawValue: "searchBarFavoriteOfflineCancel"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(searchFavoriteOfflineDidChange), name: NSNotification.Name(rawValue: "searchFavoriteOfflineDidChange"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        realm = try? Realm()
        favoriteBookOffline.removeAll()
        let objects = realm?.objects(FavoriteBookOffline.self)
        for object in objects! {
            favoriteBookOffline.append(object)
        }
        self.stories = self.favoriteBookOffline
        tableFavoriteOffline.reloadData()
    }
    func searchBarFavoriteOfflineCancel() {
        self.stories = []
        self.stories = self.favoriteBookOffline
        self.tableFavoriteOffline.reloadData()
    }
    func searchFavoriteOfflineDidChange(notification:NSNotification) {
        if let textInput = notification.userInfo?["text"] as? String{
            self.stories = []
            for story in self.favoriteBookOffline {
                if story.name.uppercased().contains(textInput.uppercased()) {
                    self.stories.append(story)
                }
            }
            self.tableFavoriteOffline.reloadData()
            
        }
    }
    func sortName() {
        let tempFavoriteBookOffline = stories.sorted { removeVietnamese(str: $0.name) < removeVietnamese(str: $1.name) }
        stories = tempFavoriteBookOffline
        tableFavoriteOffline.reloadData()
    }
    func sortTime() {
        for item in stories {
            print(item.like_time)
        }
        print("sort")
        let tempFavoriteBookOffline = stories.sorted { $0.like_time < $1.like_time }
        stories = tempFavoriteBookOffline
        for item in stories {
            print(item.like_time)
        }
        tableFavoriteOffline.reloadData()
    }
    
    func removeVietnamese(str: String) -> String {
        let vi = ["àảãáạăằẳẵắặâầẩẫấậÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ","èẻẽéẹêềểễếệÈẺẼÉẸÊỀỂỄẾỆ","ìỉĩíịÌỈĨÍỊ","òỏõóọôồổỗốộơờởỡớợÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ","ùủũúụưừửữứựÙỦŨÚỤƯỪỬỮỨỰ","ỳỷỹýỵỲỶỸÝỴ","đĐ"]
        let en = ["a", "e", "i", "o", "u", "y", "d"]
        var newStr = str
        for i in 0 ... vi.count - 1 {
            let old:String = vi[i]
            for j in 0 ... old.characters.count - 1 {
                newStr = newStr.replacingOccurrences(of: "\(old[j])", with: en[i])
            }
        }
        newStr = newStr.lowercased()
        
        return newStr
    }
    
    func convert(from offlineStory: OfflineStory) -> TopBook {
        let topBook = TopBook()
        
        topBook.id = offlineStory.id
        topBook.name = offlineStory.name
        topBook.image = offlineStory.image
        topBook.description = offlineStory.bookDescription
        topBook.book_source = offlineStory.book_source
        topBook.author = offlineStory.author
        topBook.country_id = offlineStory.country_id
        topBook.category_id = offlineStory.category_id
        topBook.page_number = offlineStory.page_number
        topBook.publisher = offlineStory.publisher
        topBook.publish_date = offlineStory.publish_date
        topBook.create_date = offlineStory.create_date
        topBook.last_update = offlineStory.last_update
        topBook.isbn = offlineStory.isbn
        topBook.language_type = offlineStory.language_type
        topBook.book_type = offlineStory.book_type
        topBook.chap_number = offlineStory.chap_number
        topBook.num_view = offlineStory.num_view
        topBook.category_name = offlineStory.category_name
        
        return topBook
    }

}
extension FavoriteOffline: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as? FavoriteSectionTitle
         cell?.numbBook.text = String(stories.count)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Online", bundle: nil)
        let detailVC = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        let favoriteOffline = self.stories[indexPath.row]
        
        var offlineStory = OfflineStory()
        let realm = try! Realm()
        let objects = realm.objects(OfflineStory.self)
        for object in objects {
            if object.id == favoriteOffline.id {
                offlineStory = object
                break
            }
        }
        
        var bookChapters: [BookChapters] = []
        
        for chapter in offlineStory.chapters {
            let bookChapter = BookChapters()
            bookChapter.chap_name = chapter.chap_name
            bookChapter.chap_no = chapter.chap_no
            bookChapter.content = chapter.content
            bookChapter.file_audio = chapter.file_audio
            
            bookChapters.append(bookChapter)
        }
        
        detailVC.isOffline = true
        detailVC.data = self.convert(from: offlineStory)
        detailVC.bookChapters = bookChapters
        detailVC.checkView = false
        detailVC.checkRecently = 1
        
        if detailVC.data.book_type == Constant.BookTypeAudio {
            detailVC.isAudioBook = true
        } else {
            detailVC.isAudioBook = false
        }
        
        if let imageData = offlineStory.imageData {
            detailVC.imageStory = UIImage(data: imageData as Data)
        }

        navigationController?.pushViewController(detailVC, animated: true)
        
        if let FavoriteVC = self.parent as? FavoriteVC {
            FavoriteVC.endSearchBar()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stories.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableFavoriteOffline.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FavoriteOfflineCell
        cell.nameBook.text = stories[indexPath.row].name
        let stringURL = stories[indexPath.item].image
        if stringURL != ""{
            let url = URL(string: "\(baseURL)\(stringURL)")
            cell.imageBook.kf.setImage(with: url)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}
