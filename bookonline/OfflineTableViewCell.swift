//
//  OfflineTableViewCell.swift
//  bookonline
//
//  Created by Enrik on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import RealmSwift

class OfflineTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelGenre: UILabel!

    @IBOutlet weak var labelAuthor: UILabel!
    
    @IBOutlet weak var buttonFavorite: UIButton!
    
    var kAuthor = R.string.bookonline.author()
    var kGenre = R.string.bookonline.kCategory()
    
    var isFavorited: Bool = false
    var offlineStory: OfflineStory!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        buttonFavorite.addTarget(self, action: #selector(actionFavorite), for: .touchUpInside)
    }
    
    func configure(story: OfflineStory) {
        
        self.offlineStory = story
        
        self.labelName.text = story.name
        self.labelGenre.text = story.category_name
        self.labelAuthor.text = kAuthor + story.author
        
        self.buttonFavorite.setImage(UIImage(named: "Star_not_filled"), for: .normal)
        
        let realm = try! Realm()
        
        let objects = realm.objects(FavoriteBookOffline.self)
        
        for object in objects {
            if object.id == story.id {
                self.buttonFavorite.setImage(UIImage(named: "Star_filled"), for: .normal)
                self.isFavorited = true
            }
        }
        
    }
    
    func actionFavorite() {
        
        let realm = try! Realm()
        
        if self.isFavorited == false {
            self.isFavorited = true
            self.buttonFavorite.setImage(UIImage(named: "Star_filled"), for: .normal)
            
            let fvBook = FavoriteBookOffline()
            
            fvBook.id = self.offlineStory.id
            fvBook.name = self.offlineStory.name
            fvBook.image = self.offlineStory.image
            fvBook.bookDescription = self.offlineStory.bookDescription
            fvBook.book_source = self.offlineStory.book_source
            fvBook.author = self.offlineStory.author
            fvBook.country_id = self.offlineStory.country_id
            fvBook.category_id = self.offlineStory.category_id
            fvBook.page_number = self.offlineStory.page_number
            fvBook.publisher = self.offlineStory.publisher
            fvBook.publish_date = self.offlineStory.publish_date
            fvBook.create_date = self.offlineStory.create_date
            fvBook.last_update = self.offlineStory.last_update
            fvBook.isbn = self.offlineStory.isbn
            fvBook.language_type = self.offlineStory.language_type
            fvBook.book_type = self.offlineStory.book_type
            fvBook.chap_number = self.offlineStory.chap_number
            fvBook.num_view = self.offlineStory.num_view
            fvBook.category_name = self.offlineStory.category_name
            fvBook.like_time = Int(Date().timeIntervalSince1970)
            try! realm .write {
                
                realm.add(fvBook)
            }
            
        } else {
    
            self.isFavorited = false
            self.buttonFavorite.setImage(UIImage(named: "Star_not_filled"), for: .normal)
            
            let objects = realm.objects(FavoriteBookOffline.self)
            for object in objects {
                if object.id == self.offlineStory.id {
                    try! realm.write {
                        realm.delete(object)
                    }
                    break 
                }
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
