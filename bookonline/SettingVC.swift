//
//  SettingVC.swift
//  bookonline
//
//  Created by tubjng on 1/11/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class SettingVC: UITableViewController {
    @IBOutlet var sizeNumber: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(self.downloadSize())
        sizeNumber.text = "\((downloadSize())/1024)"

    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  indexPath.section == 1  && indexPath.row == 0 {
            print("1234")
        }
        else if indexPath.section == 1 && indexPath.row == 1{
        print("321")
        }
        else if indexPath.section == 2 && indexPath.row == 0 {
            print("hien tai chuwa co")
            
        }
        else if indexPath.section == 2 && indexPath.row == 1 {
            print("hien tai chuwa co")
            
        }
    }
    
    func downloadSize() -> Int{
        
        let realm = try! Realm()
        
        let objects = realm.objects(OfflineStory.self)
        
        if objects.count == 0 {
            return 0
        }
        
        if let size = UserDefaults.standard.value(forKey: k_Size) {
            return size as! Int
        }
        
        return 0
    }

}
