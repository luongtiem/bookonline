//
//  Multilanguage.swift
//  bookonline
//
//  Created by tubjng on 2/17/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class Multilanguage: UITableViewController {

//    var preferredLanguage: String!
//    var perferredBundle: Bundle!
    var localeString:String?
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(languageDidChangeNotification(_:)), name: NSNotification.Name(rawValue: "LANGUAGE_DID_CHANGE"), object: nil)
        languageDidChange()
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  indexPath.section == 0  && indexPath.row == 0 {
            localeString = "en"
            if localeString != nil {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "LANGUAGE_WILL_CHANGE"), object: localeString)
            }
        }
        else if indexPath.section == 0 && indexPath.row == 1{
            localeString = "vi"
            if localeString != nil {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "LANGUAGE_WILL_CHANGE"), object: localeString)
            }
            
        }
    }
    func languageDidChangeNotification(_ notification:Notification){
        languageDidChange()
    }
    
    func languageDidChange(){
        view.window?.rootViewController = R.storyboard.main.mainVC()
    }
}
