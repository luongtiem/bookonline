//
//  CustomButton.swift
//  bookonline
//
//  Created by ReasonAmu on 1/13/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        drawBoder()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        drawBoder()
    }
    
    
    func drawBoder() {
        
        self.contentEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8)
        layer.cornerRadius = 8
        self.setTitleColor(UIColor.white, for: .selected)
    }
}
