//
//  SetupContent.swift
//  bookonline
//
//  Created by ReasonAmu on 1/16/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit
import TGPControls

class SetupContent: NSObject {
    
    var leftMenu : LeftMenuReadBook?
    var readBook : ReadBookVC?
    var check : Bool = true
    var loadDefaultLocal = UserDefaults.standard
    
    
    override init() {
        super.init()
        loadUI()
    }
    
    
    
    
    func handelTapScreen(){
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (self.readBook?.chapters)!)
            self.viewsSetupContent.removeFromSuperview()
            
            
        }, completion: nil)
        
        
    }
    
    
    let viewsSetupContent : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor.lightGray
        return v
    }()
    var slider: TGPDiscreteSlider = TGPDiscreteSlider()
    
    let btnDayBackground : WebButton = WebButton()
    let  btnNightBackground : WebButton = WebButton()
    
    let  font1 : WebButton  = WebButton()
    let  font2 : WebButton  = WebButton()
    let  font3 : WebButton  = WebButton()
    let  font4 : WebButton  = WebButton()
    
    let labelSmall : UILabel = {
        let label = UILabel()
        label.text =  R.string.bookonline.aa()
        label.sizeToFit()
        label.textColor = UIColor.darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    
    let labelBig : UILabel = {
        let label = UILabel()
        label.text =  R.string.bookonline.aa()
        label.sizeToFit()
        label.textColor = UIColor.darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 22)
        return label
    }()
    
    
    
    let blackView = UIView()
    
    
    
    func showSetupContent() {
        
        
        
        if let window = UIApplication.shared.keyWindow {
            
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            let tapScreen = UITapGestureRecognizer(target: self, action: #selector(handelTapScreen))
            tapScreen.numberOfTapsRequired = 1
            blackView.addGestureRecognizer(tapScreen)
            window.addSubview(blackView)
            
            
            self.blackView.translatesAutoresizingMaskIntoConstraints = false
            self.blackView.leadingAnchor.constraint(equalTo: window.leadingAnchor).isActive = true
            self.blackView.rightAnchor.constraint(equalTo: window.rightAnchor).isActive = true
            self.blackView.topAnchor.constraint(equalTo: window.topAnchor).isActive = true
            self.blackView.bottomAnchor.constraint(equalTo: window.bottomAnchor).isActive = true
            self.blackView.alpha = 1
            
            
            
            btnDayBackground.setTitle("Day", for: .normal)
            btnDayBackground.addTarget(self, action: #selector(handleBackground), for: .touchUpInside)
            
            
            btnNightBackground.setTitle("Night", for: .normal)
            btnNightBackground.addTarget(self, action: #selector(handleBackground), for: .touchUpInside)
            
            font1.setTitle( "Andada", for: .normal)
            font1.addTarget(self, action: #selector(handleFont), for: .touchUpInside)
            font2.setTitle( "Lato", for: .normal)
            font2.addTarget(self, action: #selector(handleFont), for: .touchUpInside)
            font3.setTitle( "Lora", for: .normal)
            font3.addTarget(self, action: #selector(handleFont), for: .touchUpInside)
            font4.setTitle( "Raleway", for: .normal)
            font4.addTarget(self, action: #selector(handleFont), for: .touchUpInside)
            
            window.addSubview(viewsSetupContent)
            viewsSetupContent.bottomAnchor.constraint(equalTo: window.bottomAnchor).isActive = true
            viewsSetupContent.leadingAnchor.constraint(equalTo: window.leadingAnchor).isActive = true
            viewsSetupContent.trailingAnchor.constraint(equalTo: window.trailingAnchor).isActive = true
            viewsSetupContent.heightAnchor.constraint(equalTo: window.heightAnchor, multiplier: 0.25).isActive = true
            
            
            viewsSetupContent.addSubview(btnDayBackground)
            btnDayBackground.topAnchor.constraint(equalTo: viewsSetupContent.topAnchor).isActive = true
            btnDayBackground.leadingAnchor.constraint(equalTo: viewsSetupContent.leadingAnchor).isActive = true
            btnDayBackground.heightAnchor.constraint(equalTo: viewsSetupContent.heightAnchor, multiplier: 1/3, constant: -1).isActive = true
            btnDayBackground.widthAnchor.constraint(equalTo: viewsSetupContent.widthAnchor, multiplier: 0.5, constant: -0.5).isActive = true
            
            viewsSetupContent.addSubview(btnNightBackground)
            btnNightBackground.topAnchor.constraint(equalTo: viewsSetupContent.topAnchor).isActive = true
            btnNightBackground.trailingAnchor.constraint(equalTo: viewsSetupContent.trailingAnchor).isActive = true
            btnNightBackground.bottomAnchor.constraint(equalTo: btnDayBackground.bottomAnchor).isActive = true
            btnNightBackground.widthAnchor.constraint(equalTo: viewsSetupContent.widthAnchor, multiplier: 0.5, constant: -0.5).isActive = true
            
            
            viewsSetupContent.addSubview(font1)
            font1.topAnchor.constraint(equalTo: btnDayBackground.bottomAnchor, constant:1).isActive = true
            font1.leadingAnchor.constraint(equalTo: btnDayBackground.leadingAnchor).isActive = true
            font1.heightAnchor.constraint(equalTo: viewsSetupContent.heightAnchor, multiplier: 1/3, constant: -1).isActive = true
            font1.widthAnchor.constraint(equalTo: viewsSetupContent.widthAnchor, multiplier: 0.25, constant: 0).isActive = true
            
            viewsSetupContent.addSubview(font2)
            font2.topAnchor.constraint(equalTo: font1.topAnchor).isActive = true
            font2.bottomAnchor.constraint(equalTo: font1.bottomAnchor).isActive = true
            font2.leftAnchor.constraint(equalTo: font1.rightAnchor, constant: 0).isActive = true
            font2.widthAnchor.constraint(equalTo: viewsSetupContent.widthAnchor, multiplier: 0.25, constant: 0).isActive = true
            
            viewsSetupContent.addSubview(font3)
            font3.topAnchor.constraint(equalTo: font1.topAnchor).isActive = true
            font3.bottomAnchor.constraint(equalTo: font1.bottomAnchor).isActive = true
            font3.leftAnchor.constraint(equalTo: font2.rightAnchor, constant: 0).isActive = true
            font3.widthAnchor.constraint(equalTo: viewsSetupContent.widthAnchor, multiplier: 0.25, constant: 0).isActive = true
            
            viewsSetupContent.addSubview(font4)
            font4.topAnchor.constraint(equalTo: font1.topAnchor).isActive = true
            font4.bottomAnchor.constraint(equalTo: font1.bottomAnchor).isActive = true
            font4.leftAnchor.constraint(equalTo: font3.rightAnchor, constant: 0).isActive = true
            font4.widthAnchor.constraint(equalTo: viewsSetupContent.widthAnchor, multiplier: 0.25, constant: 0).isActive = true
            
            
            let viewBot = UIView()
            viewBot.translatesAutoresizingMaskIntoConstraints = false
            viewBot.backgroundColor = UIColor.white
            
            viewsSetupContent.addSubview(viewBot)
            viewBot.leadingAnchor.constraint(equalTo: viewsSetupContent.leadingAnchor).isActive = true
            viewBot.trailingAnchor.constraint(equalTo: viewsSetupContent.trailingAnchor).isActive = true
            viewBot.bottomAnchor.constraint(equalTo: viewsSetupContent.bottomAnchor).isActive = true
            viewBot.topAnchor.constraint(equalTo: font1.bottomAnchor, constant: 1).isActive = true
            
            viewBot.addSubview(labelBig)
            labelBig.rightAnchor.constraint(equalTo: viewBot.rightAnchor, constant: -20).isActive = true
            labelBig.centerYAnchor.constraint(equalTo: viewBot.centerYAnchor, constant: 0).isActive = true
            
            viewBot.addSubview(labelSmall)
            labelSmall.leftAnchor.constraint(equalTo: viewBot.leftAnchor, constant: 20).isActive = true
            labelSmall.centerYAnchor.constraint(equalTo: labelBig.centerYAnchor, constant: 0).isActive = true
            
            
                slider.backgroundColor = UIColor.white
            
            viewBot.addSubview(slider)
            slider.translatesAutoresizingMaskIntoConstraints = false
            slider.leftAnchor.constraint(equalTo: labelSmall.rightAnchor, constant: 10).isActive = true
            slider.rightAnchor.constraint(equalTo: labelBig.leftAnchor, constant: -10).isActive = true
            slider.topAnchor.constraint(equalTo: viewBot.topAnchor).isActive = true
            slider.bottomAnchor.constraint(equalTo: viewBot.bottomAnchor).isActive = true

            
            slider.tickStyle = 4
            slider.tickSize = CGSize(width: 1, height: 20)
            slider.tickCount = 4
            slider.tickImage = "tick"
            slider.trackStyle = 0
            slider.thumbImage = "thumb"
            slider.minimumValue = 1
            slider.thumbStyle = 4
            slider.incrementValue = 1
            slider.minimumTrackTintColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
            slider.addTarget(self, action: #selector(handleSlider), for: .valueChanged)
            
        }
        
    }
    
    
    func loadUI(){
        
       let  size = UserDefaults.standard.value(forKey: "size") as! String!
       let  backGround = UserDefaults.standard.value(forKey: "backGround") as! String!
       let  font_Family = UserDefaults.standard.value(forKey: "font_Family") as! String!
        
        if backGround == "black" {
            btnNightBackground.setTitleColor(UIColor.blue, for: .normal)
        }else{
            btnDayBackground.setTitleColor(UIColor.blue, for: .normal)
        }
        
        //-- font
        if font_Family ==  "Andada-Regular" {
            font1.setTitleColor(UIColor.blue, for: .normal)
        }else if font_Family == "Lato-Regular" {
            font2.setTitleColor(UIColor.blue, for: .normal)
        }else if font_Family == "Lora-RegularF" {
            font3.setTitleColor(UIColor.blue, for: .normal)
        }else {
            font4.setTitleColor(UIColor.blue, for: .normal)
        }
        
        //--
        
        if size == "100%" {
            slider.value = 1
        }else if size == "125%" {
            slider.value = 2
        }else if size == "150%"{
            slider.value = 3
        }else {
            slider.value = 4
        }
    }
    
}



extension SetupContent {
    
    
    
    func handleSlider() {
        setupFontSize(size: Int(slider.value))
        
    }
    
    func setupFontSize(size : Int){
        
        switch size {
        case 1:
            readBook?.size = "100%"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
        case 2:
            readBook?.size = "125%"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
        case 3:
            readBook?.size = "150%"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
        case 4:
            readBook?.size = "175%"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
        default:
            break
        }
    
    }
    
    func handleBackground(){
        if btnNightBackground.isTouchInside == true {
            btnDayBackground.setTitleColor(UIColor.darkGray, for: .normal)
            btnNightBackground.setTitleColor(UIColor.blue, for: .normal)
            readBook?.backGround = "black"
            readBook?.color = "white"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
        }else{
            btnDayBackground.setTitleColor(UIColor.blue, for: .normal)
            btnNightBackground.setTitleColor(UIColor.darkGray, for: .normal)
            readBook?.backGround = "white"
            readBook?.color = "black"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
        }
        
        
    }
    
    func handleFont(){
        
        if font1.isTouchInside == true {
            chooseFont(type: .font1)
        }else if font2.isTouchInside == true {
            chooseFont(type: .font2)
        }else if font3.isTouchInside == true {
            chooseFont(type: .font3)
        }else{
            chooseFont(type: .font4)
        }
        
    }
    
    func chooseFont(type : font){
        
        switch type {
        case .font1:
            font1.setTitleColor(UIColor.blue, for: .normal)
            font2.setTitleColor(UIColor.darkGray, for: .normal)
            font3.setTitleColor(UIColor.darkGray, for: .normal)
            font4.setTitleColor(UIColor.darkGray, for: .normal)
            readBook?.font_Family = "Andada-Regular"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
        case .font2:
            font2.setTitleColor(UIColor.blue, for: .normal)
            font1.setTitleColor(UIColor.darkGray, for: .normal)
            font3.setTitleColor(UIColor.darkGray, for: .normal)
            font4.setTitleColor(UIColor.darkGray, for: .normal)
            readBook?.font_Family = "Lato-Regular"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
        case .font3:
            font3.setTitleColor(UIColor.blue, for: .normal)
            font1.setTitleColor(UIColor.darkGray, for: .normal)
            font2.setTitleColor(UIColor.darkGray, for: .normal)
            font4.setTitleColor(UIColor.darkGray, for: .normal)
            readBook?.font_Family = "Lora-RegularF"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
            
        case .font4:
            font4.setTitleColor(UIColor.blue, for: .normal)
            font1.setTitleColor(UIColor.darkGray, for: .normal)
            font2.setTitleColor(UIColor.darkGray, for: .normal)
            font3.setTitleColor(UIColor.darkGray, for: .normal)
            readBook?.font_Family = "Raleway-Light"
            self.readBook?.setupUI(size: (self.readBook?.size)!, color: (self.readBook?.color)!, background:(self.readBook?.backGround)!, font_Family: (self.readBook?.font_Family)!,chapter : (readBook?.chapters)!)
            
        }
    }
    
}





enum font : Int {
    
    case font1 = 0
    case font2
    case font3
    case font4
}

class WebButton : UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    func setupUI(){
        self.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        self.setTitleColor(UIColor.darkGray, for: .normal)
        self.backgroundColor = UIColor.white
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
