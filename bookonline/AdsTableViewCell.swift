//
//  AdsTableViewCell.swift
//  bookonline
//
//  Created by tubjng on 2/14/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class AdsTableViewCell: UITableViewCell {
    @IBOutlet var bannerAds: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
