//
//  RecentlyOnlineCell.swift
//  bookonline
//
//  Created by tubjng on 1/10/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import UIKit

class RecentlyOnlineCell: UITableViewCell {
    @IBOutlet var nameTitle: UILabel!
    @IBOutlet var author: UILabel!
    
    @IBOutlet var recentlyTime: UILabel!
    
    @IBOutlet var imageBook: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func display(recentlyBookOffline: RecentlyBookOffline) {
        let secs = Int(Date().timeIntervalSince1970) - recentlyBookOffline.last_time
        recentlyTime.text = toDateAgo(from: secs)
    }
    
    func display(recentlyBook: RecentlyBook) {
        let secs = Int(Date().timeIntervalSince1970) - recentlyBook.last_time
        recentlyTime.text = toDateAgo(from: secs)
    }
    
    func toDateAgo(from seconds: Int) -> String {
        var time = ""
        if seconds < 60 {
            time = "\(seconds) \(R.string.bookonline.secondAgo())"
        } else if seconds < 3600 {
            let minutes = seconds / 60
            time = "\(minutes) \(R.string.bookonline.minuteAgo())"
        } else if seconds < 86400 {
            let hours = seconds / 3600
            time = "\(hours) \(R.string.bookonline.houAgor())"
        } else {
            let days = seconds / 86400
            if days < 4 {
                time = "\(days) \(R.string.bookonline.dayAgo())"
            } else {
                let date = Date(timeIntervalSinceNow: TimeInterval(-seconds))
                let calendar = Calendar.current
                let components = calendar.dateComponents([.day, .month, .year], from: date)
                let month = components.month
                let day = components.day
                time = "\(day!) \(R.string.bookonline.day()), \(month!) \(R.string.bookonline.month())"
            }
        }
        return time
    }
}
