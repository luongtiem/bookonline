//
//  Constant.swift
//  bookonline
//
//  Created by Enrik on 1/23/17.
//  Copyright © 2017 ReasonAmu. All rights reserved.
//

import Foundation

struct Constant {
    static let TimeJump = 5
    static let RepeatNone = 0
    static let RepeatOne = 1
    static let RepeatAll = 2
    
    static let BookTypeAudio = "1"
    static let BookTypeText = "0"
}
